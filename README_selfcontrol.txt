This is a Howto meant for people who want to use Webcontentcontrol for self-control:
====================================================================================

I'm not really proud of having to resort to such extreme measures, but I know I'm not the only one wasting time on the net. ;)

This is currently just a first draft which will be expanded later and should get into the help documentation once I get to writing it. ;)

The main idea is to:
1)edit /etc/sudoers so that you only have access to a limited set of commands as superuser
2)Create a random root password using "makepasswd" or "pwgen" for example.

To keep the root password safe (because you might need it again), you can:
-Ask someone you know to keep it safe for you.
-Put it in a physically remote location.
-Encrypt it using gpg, store it in /root for example with 000 permissions and set up a cron job to make it readable by you after a given time. (yes, I know, this is crazy. But it works.)
-Store the gpg key on a mail account to which you ban yourself access from through dansguardian. This solution requires proxy lockdown in firefox and disabling/proxy locking of all other browsers since most mail providers use SSL.

I've already partially automated the process myself using the following scripts which come with Webcontentcontrol:

===========
/usr/share/webcontentcontrol/scripts/totalcontrol_lock.sh
/usr/share/webcontentcontrol/scripts/totalcontrol_unlock.sh
/usr/share/webcontentcontrol/scripts/keepsafe_until.sh
===========

Files to customize:
===========
scripts/sudoers.locked.template
scripts/crontab.locked.template
===========

ex:
1)sudo ./totalcontrol_lock.sh USERNAME /root/ROOTPASSWORDFILE.gpg \* \* 1 \* \*
This will lock down the system completely using the template files mentioned above until the first day of the next month.
Note the "\" in front of "*" to get the "*" through.
It will give ROOTPASSWORDFILE 000 permissions and add a line to crontab to free it using the given arguments.

2)sudo ./totalcontrol_lock.sh USERNAME
This will do the same, except it won't lock down a file and add a line to crontab to free it at a given time.

SCRIPTS:
========
I wrote a lot of scripts specific to self-control.
I'll try to document them as soon as I can. If you have any questions concerning the scripts, just ask me.
Help is welcome for writing documentation too by the way. ;)

Note 1:
I previously used "gpg -e -r MAIL" for encryption, but I discovered that using "gpg -e -c -r MAIL" is better since it allows decrypting the encrypted file on another PC.
I think this is safer since if you loose your gpg keys (in ~/.gnupg), you loose your root password. :/

Note 2:
I recommend disabling the ability to install/remove/upgrade software.
-Upgrades can break the dansguardian+tinyproxy+firehol config and either disable the protection or prevent internet access completely.
-Installation of new software might enable circumventing the protection.
-Removal of dansguardian, tinyproxy and firehol might disable the protection or prevent internet access completely.

SELF-CONTROL FOR EXTREME CASES:
===============================
There are 3 levels of security on most PCs with GNU/Linux:
1)BIOS password
2)GRUB password
3)root password

No BIOS password allows booting from a Live-CD or a USB-key.
No GRUB password can allow booting into an unprotected OS (ex: Windows) or into failsafe mode which can eventually grant root permissions.
And the root password is necessary since you don't want to be able to disable the protection.

So you will actually need to:
1) Disable boot from anything else than the HD in the BIOS and password-protect it.
2) password protect all entries in GRUB except the main one.
3) Set up a root password.

