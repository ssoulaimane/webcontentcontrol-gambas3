��    O      �  k         �  	   �  
   �     �     �  
   �     �     �     �     �  	   �               '     6     <     I     U     c     {     �     �     �     �     �     �     �     �     �               &     3     ;     O     n     r          �     �     �     �     �     �     �     �     		  	   	     	     8	     =	  	   K	     U	     ]	     i	     l	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     	
     
  {   0
  	   �
     �
     �
     �
     �
     �
          !  &   *  �  Q               "     *     6     E  	   L     V     Z  	   ]     g     ~     �     �     �     �     �     �     �          &     8     @     V     m     �  	   �     �     �     �     �     �     �     �                    0     8     @     W     g     x     �     �     �     �     �  	   �     �  
                  2  #   5     Y     t     |     �     �     �     �     �     �     �     �     �  "   �  �     	   �     �     �  
   �  "   �           =     Q  0   b     J   '   +                  *      :      M       "                     #   /                     4       E   ?         .          1          	           0                A   O       H   3             7       6          I      %                           2   )   G   >       
   F   N   5   !   <   D           9   C   K      B       ,            ;          (   &      @   L   $   =       8   -    &About... &Export... &File &Help &Import... &Options &Quit ... => AdBlocker Advanced settings Allowed Phrases Allowed groups Apply Banned Sites Banned URLs Banned groups Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Configure BLACKLISTS Configure WHITELISTS Custom Custom: DansGuardian Description Disable Dansguardian Edit current Editor: Enable Dansguardian Enter your description here... FAQ Filebrowser: Filter settings FireHOL FireHol Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language: Load selected configuration Logs Logs unlocked Manual... Minimal Netbrowser: OK Open in external browser Open log directory Other Protection Proxy port: Refresh Reload Report a bug... Save Services Setting description Sites Not Filtered Sudo frontend: Test access denied page This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy Troubleshooting URLs Not Filtered User: View in external browser View with text editor Web Content Control Websites tinyproxy+firehol+firefox config files Project-Id-Version: webcontentcontrol
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-09-08 14:13+0000
Last-Translator: Vojtěch Trefný <vojtech.trefny@gmail.com>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-09-25 22:56+0000
X-Generator: Launchpad (build Unknown)
 &O aplikaci &Exportovat... &Soubor &Nápověda &Importovat... &Volby &Ukončit ... => AdBlocker Pokročilá nastavení Povolené fráze Povolené skupiny Použít Zakázané stránky Zakázané adresy Zakázané skupiny Blokované typy souborů Zakázané IP adresy Blokované MIME typy Blokované fráze Zrušit Zobrazit atávající BLACKLIST - nastavení WHITELIST - nastavení Vlastní Vlastní: DansGuardian Popis Zakázat Dansguardian Upravit stávající Editor: Povolit Dansguardian Sem vložte popis... FAQ Správce souborů: Nastavení filtru FireHOL FireHol Gnome (gksudo + gedit) Gnome nebo KDE? Gnome nebo KDE?: Nefiltrované IP adresy Filtrování obrázků KDE (kdesudo + kate) Jazyk Jazyk: Načíst zvolenou konfiguraci Protokoly Protokoly odemčeny Manuál... Minimální Webový prohlížeč: OK Otevřít v externím prohlížeči Otevřít protokol přímo Další Ochrana Port proxy: Obnovit Obnovit Nahlásit chybu... Uložit Služby Popis nastavení Nefiltrované stránky Sudo frontend: Test stránky "přístup odepřen" Toto je grafické rozhraní určené pro snadné nastavení, spuštění a zastavení kontroly webového obsahu založené na DansGuardian, TinyProxy a FireHol. TinyProxy Řešení problémů Nefiltrované adresy Uživatel: Zobrazit v externím prohlížeči Zobrazit v textovém editoru Web Content Control Webové stránky konfigurační soubory tinyproxy+firehol+firefox 