��    }        �   �      �
  	   �
  
   �
     �
     �
  
   �
     �
     �
  �   �
  u   N  �   �     H     L  	   O  )   Y     �     �     �     �     �     �     �     �     �     	  $        <     T     `     s     �     �  $   �     �     �     �     �                    .     N     Z     o     �     �     �     �     �     �     �     �     �               ,     G     ^     l     {     �     �     �     �     �     �  	   �     �          7     <  	   J     T     \     n     �     �     �     �     �     �     �     �     �           .     4     B  {   X  z   �  �   O  v   �     O  
   c     n     z     �     �     �     �     �     �     �     �  $   �     "     )     D     `     o  {   �  	             *     =     M     _     }     �     �     �     �     �     �  &   �  �       �  
          
     
     
   *  
   5  �   @  �   �  �   d     �     �     �  "        5     M     ^     p     y     �     �     �     �     �  .        4     Q     f     {     �     �  2   �     �     �            	   6     @     M  !   h     �     �     �     �     �     �     �     �          -     :     Q     Y  $   a     �     �     �     �     �     �  "        8     L     b     w     }  !   �  $   �     �     �     �  
   �                /     C     J     ]     p          �      �  !   �  '   �            	         ~   8   ~   �   s   6!  r   �!     "     4"     <"     H"     Q"     Z"     t"     �"  (   �"     �"     �"     �"     �"     �"  !   #  "   )#     L#  +   `#  �   �#  	   .$     8$     W$     p$     �$  $   �$     �$     �$  !   �$     �$     %     4%     N%  0   _%         ^   !          D   y         H              [   {   P   m      .          W   R                              :      v              q   c   &   j   g   d   `             e      ?   F   a   o   J   $   %   b      G      h       @      Q   u   k          (   6   L       t       E   A   _   M   '   Y          I   f   l   0      x   \   <   U           p       n   T   2   >   ]   Z   "       3   4       	       ;       N           5   7       8   
   /       9       -       |             +       X          ,   i   1       #   S   K   }   r   w       =      )       s      C   O       B               *       z   V                   &About... &Export... &File &Help &Import... &Options &Quit * Moderate Image Filtering

* Moderate File Extension Blocking

* Moderate MIME Type Blocking

* Naughtyness Limit set for older children * No Image Filtering

* No File Extension Blocking

* No MIME Type Blocking

* Naughtyness Limit set for young adults * Strict Image Filtering

* Strict File Extension Blocking

* Strict MIME Type Blocking

* Naughtyness Limit set for young children ... => AdBlocker Admin mode (i.e. ask me for the password) Advanced settings Allowed Phrases Allowed groups Apply Apply all settings Apply port settings Auto set ports Banned Sites Banned URLs Banned groups Blacklists (can take a lot of space) Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Check optional items to export here: Configure BLACKLISTS Configure WHITELISTS Custom Custom apps settings Custom: DansGuardian DansGuardian defaults Dansguardian running at startup Description Disable Dansguardian Disable status refresh Edit current Editor: Enable Dansguardian Enter your description here... FAQ Filebrowser: Filter port: Filter settings FireHOL FireHol Firefox proxy settings unlocked Firehol running at startup Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language: Load selected configuration Lock Firefox Proxy Settings Logs Logs unlocked Manual... Minimal Minimal Filtering Minimal Filtering: Misc. Settings Moderate Moderate Filtering Moderate Filtering: Naughtyness Limit Netbrowser: OK Open in external browser Open log directory Open main configuration files... Other Port settings Preset configurations Process Complete. Dansguardian has now been completely disabled. You should restart any web browsers that you have running. Process Complete. Dansguardian has now been completely enabled. You should restart any web browsers that you have running. Process Complete. You should now be able to adjust your Firefox proxy settings. You must restart Firefox for the changes to take effect. Process Complete. Your Firefox proxy settings are now locked. You must restart Firefox for the changes to take effect. Program permissions Protection Proxy port: Refresh Reload Reload current ports Report a bug... Save Save custom apps settings Services Setting description Sites Not Filtered Some description of the settings. :) Strict Strict (Default) Filtering Strict (Default) Filtering: Sudo frontend: Test access denied page This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy Tinyproxy running at startup Totalcontrol files Troubleshooting URLs Not Filtered Unlock Firefox Proxy Settings User: View &console View in external browser View with text editor WPA interfaces unlocked Web Content Control Websites tinyproxy+firehol+firefox config files Project-Id-Version: webcontentcontrol
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-06-19 22:15+0000
Last-Translator: Peter Mráz <Unknown>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-09-25 23:20+0000
X-Generator: Launchpad (build Unknown)
X-Poedit-Country: Slovakia
X-Poedit-Language: slovak
 &O programe... &Export... &Súbor &Pomocník &Import... &Možnosti &Ukončiť * Stredné filtrovanie obrázkov

* Stredné blokovanie prípon súborov

* Stredné blokovanie MIME Typov

* Typ obmedzenia nastavené pre staršie deti * Bez filtrovania obrázkov

* Bez blokovania prípon súborov

* Bez blokovania MIME Typov

* Typ obmedzenia nastavený pre adolescentov * Prísne filtrovanie obrázkov

* Prísne blokovanie prípon súborov

* Prísne blokovanie MIME Typov

* Typ obmedzenia nastavené pre malé deti ... => Blokovanie reklamy Mód správcu (spýta sa na heslo) Rozšírené nastavenia Povolené frázy Povolené skupiny Použiť Použiť všetky nastavenia Použiť nastavenie portov Automaticky nastaviť porty Zakázané weby Zakázané URL adresy Zakázané skupiny Čierne listiny (môžu zaberať veľa miesta) Blokované prípony súborov Zakázané IP adresy Blokované MIME typy Blokované frázy Zrušiť Skontrolovať Zaškrtnite položky, ktoré chcete vyexportovať: Nastavenie čiernej listiny Nastavenie bielej listiny Vlastné Vlastné nastavenie aplikácií Vlastné: DansGuardian Predvolené DansGuardianom Dansguardian spustiť pri štarte Popis Vypnutie Dansguardianu Vypnúť obnovovanie stavu Upraviť Editor: Zapnutie Dansguardianu Sem zadajte popis... Často kladené otázky Prehliadač súborov: Port filtra: Nastavenie filtrovania FireHOL FireHol Proxy nastavenia Firefoxu odomknuté Firehol spustiť pri štarte Gnome (gksudo + gedit) Gnome alebo KDE? Gnome alebo KDE?: Pomôcť s  &programovaním... Pomôcť s &prekladom... Pomôcť s testovaním &vydaní... Povolené IP adresy Filtrovanie obrázkov KDE (kdesudo + kate) Jazyk Jazyk: Načítať vybranú konfiguráciu Uzamknutie proxy nastavení Firefoxu Záznamy Záznam odomknutý Príručka... Minimálne Minimálne filtrovanie Minimálne filtrovanie: Ostatné nastavenia Mierne Mierne filtrovanie Mierne filtrovanie Typ obmedzenia Prehliadač siete: OK Otvoriť v externom prehliadači Otvoriť priečinok so záznamami Otvoriť hlavné konfiguračné súbory Ostatné Nastavenie portov Prednastavená konfigurácia Proces ukončený. Dansguardian je teraz vypnutý, Teraz by ste mali reštartovať všetky prehliadače, ktoré sú otvorené. Proces ukončený. Dansguardian je teraz zapnutý, Teraz by ste mali reštartovať všetky prehliadače, ktoré sú otvorené. Proces ukončený. Proxy nastavenia Firefoxu sú odomknuté. Musíte reštartovať Firefox, aby sa zmeny prejavili. Proces ukončený. Proxy nastavenia Firefoxu sú zamknuté. Musíte reštartovať Firefox, aby sa zmeny prejavili. Oprávnenia k programu Ochrana Port proxy: Obnoviť Obnoviť Obnoviť súčasné porty Nahlásiť chybu... Uložiť Uložiť vlastné nastavenie aplikácií Služby Popis nastavenia Povolené weby Krátky popis nastavení. :) Prísne Prísne (predvolené) filtrovanie Prísne (predvolené) filtrovanie: Rozhranie pre sudo: Otestovať stránku Pristup bol zakázaný! Pomocou tohto rozhrania sa veľmi jednoducho konfiguruje, zapína a vypína kontrola webového obsahu založená na systémoch DansGuardian, TinyProxy a FireHol. TinyProxy Tinyproxy spustiť pri štarte Celkový riadiaci súbor Riešenie problémov Povolené URL adresy Odomknutie Proxy nastavení Firefoxu Používateľ: View &konzolu Zobraziť v externom prehliadači Zobraziť v textovom editore WPA rozhrania odomknuté Kontrola webového obsahu Webové stránky Konfiguračné súbory tinyproxy+firehol+firefox 