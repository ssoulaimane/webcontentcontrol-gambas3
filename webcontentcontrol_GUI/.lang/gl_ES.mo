��    }        �   �      �
  	   �
  
   �
     �
     �
  
   �
     �
     �
  �   �
  u   N  �   �     H     L  	   O  )   Y     �     �     �     �     �     �     �     �     �     	  $        <     T     `     s     �     �  $   �     �     �     �     �                    .     N     Z     o     �     �     �     �     �     �     �     �     �               ,     G     ^     l     {     �     �     �     �     �     �  	   �     �          7     <  	   J     T     \     n     �     �     �     �     �     �     �     �     �           .     4     B  {   X  z   �  �   O  v   �     O  
   c     n     z     �     �     �     �     �     �     �     �  $   �     "     )     D     `     o  {   �  	             *     =     M     _     }     �     �     �     �     �     �  &   �  �       �     �  	   �     �     �  	   	       �     �   �  �   j            	   "  9   ,     f     �     �     �     �  "   �  "   �               -  (   C  "   l     �     �     �     �     �  4   �     
     #     =  &   K     r     �     �  *   �     �     �  %   �           0     8  #   M     q     u     �     �     �     �  1   �  %   �          1     ?     N     `     u     �     �     �     �     �  $   �  -   �  	   $     .  	   F     P     X     i     {     �     �     �     �     �     �     �        1   4      f      m      �   z   �   w   !  �   �!  �   ("     �"     �"     �"  
   �"     �"     �"     #     ##  0   *#     [#     d#     z#  !   �#     �#  !   �#  "   �#     �#  "   $  �   8$  	   �$  '   �$     %     )%     @%  0   S%     �%     �%     �%     �%     �%     �%  
   &  8   &         ^   !          D   y         H              [   {   P   m      .          W   R                              :      v              q   c   &   j   g   d   `             e      ?   F   a   o   J   $   %   b      G      h       @      Q   u   k          (   6   L       t       E   A   _   M   '   Y          I   f   l   0      x   \   <   U           p       n   T   2   >   ]   Z   "       3   4       	       ;       N           5   7       8   
   /       9       -       |             +       X          ,   i   1       #   S   K   }   r   w       =      )       s      C   O       B               *       z   V                   &About... &Export... &File &Help &Import... &Options &Quit * Moderate Image Filtering

* Moderate File Extension Blocking

* Moderate MIME Type Blocking

* Naughtyness Limit set for older children * No Image Filtering

* No File Extension Blocking

* No MIME Type Blocking

* Naughtyness Limit set for young adults * Strict Image Filtering

* Strict File Extension Blocking

* Strict MIME Type Blocking

* Naughtyness Limit set for young children ... => AdBlocker Admin mode (i.e. ask me for the password) Advanced settings Allowed Phrases Allowed groups Apply Apply all settings Apply port settings Auto set ports Banned Sites Banned URLs Banned groups Blacklists (can take a lot of space) Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Check optional items to export here: Configure BLACKLISTS Configure WHITELISTS Custom Custom apps settings Custom: DansGuardian DansGuardian defaults Dansguardian running at startup Description Disable Dansguardian Disable status refresh Edit current Editor: Enable Dansguardian Enter your description here... FAQ Filebrowser: Filter port: Filter settings FireHOL FireHol Firefox proxy settings unlocked Firehol running at startup Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language: Load selected configuration Lock Firefox Proxy Settings Logs Logs unlocked Manual... Minimal Minimal Filtering Minimal Filtering: Misc. Settings Moderate Moderate Filtering Moderate Filtering: Naughtyness Limit Netbrowser: OK Open in external browser Open log directory Open main configuration files... Other Port settings Preset configurations Process Complete. Dansguardian has now been completely disabled. You should restart any web browsers that you have running. Process Complete. Dansguardian has now been completely enabled. You should restart any web browsers that you have running. Process Complete. You should now be able to adjust your Firefox proxy settings. You must restart Firefox for the changes to take effect. Process Complete. Your Firefox proxy settings are now locked. You must restart Firefox for the changes to take effect. Program permissions Protection Proxy port: Refresh Reload Reload current ports Report a bug... Save Save custom apps settings Services Setting description Sites Not Filtered Some description of the settings. :) Strict Strict (Default) Filtering Strict (Default) Filtering: Sudo frontend: Test access denied page This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy Tinyproxy running at startup Totalcontrol files Troubleshooting URLs Not Filtered Unlock Firefox Proxy Settings User: View &console View in external browser View with text editor WPA interfaces unlocked Web Content Control Websites tinyproxy+firehol+firefox config files Project-Id-Version: webcontentcontrol
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-05-18 17:13+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-09-25 23:04+0000
X-Generator: Launchpad (build Unknown)
 &Acerca de... Exportar... &Ficheiro &Axuda Importar... &Opcións &Sair * Filtrado moderado de imaxes

* Bloqueo moderado de extensións de ficheiros

* Bloqueo moderado de tipos MIME

* Axustado o nivel de contidos inapropiados para nenos maiores * Sen filtrado de imaxes

* Sen bloqueo de extensiónas de ficheiros

* Sen bloqueo de tipos MIME

* Axustado o nivel de contidos inapropiados para adolescentes * Filtrado estrito de imaxes

* Bloqueo estrito de extensións de ficheiros

* Bloqueo estrito de tipos MIME

* Axustado o nivel de contidos inapropiados para cativos (miúdos) ... => AdBlocker Modo de administración (é decir, pidenme o contrasinal) Configuracións avanzadas Frases permitidas Grupos permitidos Aplicar Aplicar todos os axustes Aplicar a configuración de portos Configurar o porto automaticamente Sitios prohibidos URLs prohibidas Grupos non permitidos Listas negras (pode ocupar moito espazo) Extensións de ficheiro bloqueadas IPs bloqueadas Bloquear Frases bloqueadas Cancelar Comprobar a actual Comprobe os elementos opcionais para exportar aquí: Configurar LISTAS NEGRAS Configurar LISTAS BRANCAS Personalizado Axustes personalizados de aplicacións Personalizado: DansGuardian Predefinidos de Dansguardian Executar Dansguardian ao iniciar o sistema Descrición Desactivar Dansguardian Desactivar a actualización de estado Editar a actual Editor: Activar Dansguardian Escriba aquí a súa descrición... FAQ Navegador de ficheiros: Filtrar o porto: Axustes de filtros FireHOL FireHol Desbloqueada a configuración do proxy en Firefox Executar Firehol ao iniciar o sistema Gnome (gksudo + gedit)E Gnome or KDE? Gnome ou KDE?: Axuda &código... Axuda &tradución... Axuda proba &final IPs non filtradas Filtrado de imaxes KDE (kdesudo + kate) Idioma Idioma: Cargar a configuración seleccionada Configuración do bloqueo do proxy de Firefox Rexistros Rexistros desbloqueados Manual... Mínimo Filtrado mínimo Filtrado mínimo: Axustes (miscelánea) Moderado Filtrado moderado Filtrado moderado: Nivel de contidos inapropiados Navegador de rede: Aceptar Abrir nun navegador externo Abrir o directorio de rexistro Abrir os ficheiros de configuración principal... Outros Configurar o porto Configuracións predefinidas Proceso completado. Dansguardian foi desactivado totalmente. Debe reiniciar calquera navegador web que se este a executar. Proceso completado. Dansguardian foi activado totalmente. Debe reiniciar calquera navegador web que se este a executar. Proceso completado. Agora debe ser quen de axustar a súa configuración do proxy de Firefox. Debe reiniciar Firefox para que os cambios teñan efecto. Proceso completado.  Os axustes do proxy de Firefox están agora bloqueados. Debe reiniciar Firefox para que os cambios teñan efecto. Permisos de programas Protección Porto do proxy: Actualizar Recargar Recargar os portos actuais Informar dun fallo Gardar Gardar os axustes personalizados de aplicacións Servizos Descrición do axuste Sitios non filtrados Breve descrición dos axustes. :) Estrito Filtrado estrito (predeterminado) Filtrado estrito (predeterminado): Interface de «sudo»: Proba de acceso denegado a páxina Esta é unha interface gráfica fácil de configurar, iniciar e deter o «control de contidos web» baseado na configuración de DansGuardian, TinyProxy e FireHol. TinyProxy Executar Tinyproxy ao iniciar o sistema Control total de ficheiros Solución de problemas URLs non filtrados Configuración do desbloqueo do proxy de Firefox Usuario: Ver &consola Ver nun navegador externo Ver cun editor de texto Interfaces WPA desbloqueadas Control de contidos web Sitios web Ficheiros de configuración de tinyproxy+firehol+firefox 