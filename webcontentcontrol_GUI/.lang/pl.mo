��    }        �   �      �
  	   �
  
   �
     �
     �
  
   �
     �
     �
  �   �
  u   N  �   �     H     L  	   O  )   Y     �     �     �     �     �     �     �     �     �     	  $        <     T     `     s     �     �  $   �     �     �     �     �                    .     N     Z     o     �     �     �     �     �     �     �     �     �               ,     G     ^     l     {     �     �     �     �     �     �  	   �     �          7     <  	   J     T     \     n     �     �     �     �     �     �     �     �     �           .     4     B  {   X  z   �  �   O  v   �     O  
   c     n     z     �     �     �     �     �     �     �     �  $   �     "     )     D     `     o  {   �  	             *     =     M     _     }     �     �     �     �     �     �  &   �  �       �     �     �     �     �     �     �  �     �   �  �   5     �     �  	   �  )   �          ,     G     Y     b     �     �     �     �     �  )   �          9     O     m     �     �  5   �  $   �  %   �          &     D     R  !   _  -   �     �     �  $   �     �     �               6     :     Q     f     x     �  /   �     �     �     �     �          #     ;     Q     j     �     �     �     �  C   �          $  	   A  	   K     U     k     �     �     �     �     �     �        $          ,   #   M   
   q      |      �   �   �   �   !  �   0"  �   �"     i#     �#  )   �#  	   �#     �#     �#     �#      $     $     &$     .$     =$     S$  
   l$  "   w$  "   �$     �$  &   �$  �   %  	   �%  )   �%     �%     &     %&  7   ?&     w&     �&  (   �&     �&     �&     �&  
   '  .   '         ^   !          D   y         H              [   {   P   m      .          W   R                              :      v              q   c   &   j   g   d   `             e      ?   F   a   o   J   $   %   b      G      h       @      Q   u   k          (   6   L       t       E   A   _   M   '   Y          I   f   l   0      x   \   <   U           p       n   T   2   >   ]   Z   "       3   4       	       ;       N           5   7       8   
   /       9       -       |             +       X          ,   i   1       #   S   K   }   r   w       =      )       s      C   O       B               *       z   V                   &About... &Export... &File &Help &Import... &Options &Quit * Moderate Image Filtering

* Moderate File Extension Blocking

* Moderate MIME Type Blocking

* Naughtyness Limit set for older children * No Image Filtering

* No File Extension Blocking

* No MIME Type Blocking

* Naughtyness Limit set for young adults * Strict Image Filtering

* Strict File Extension Blocking

* Strict MIME Type Blocking

* Naughtyness Limit set for young children ... => AdBlocker Admin mode (i.e. ask me for the password) Advanced settings Allowed Phrases Allowed groups Apply Apply all settings Apply port settings Auto set ports Banned Sites Banned URLs Banned groups Blacklists (can take a lot of space) Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Check optional items to export here: Configure BLACKLISTS Configure WHITELISTS Custom Custom apps settings Custom: DansGuardian DansGuardian defaults Dansguardian running at startup Description Disable Dansguardian Disable status refresh Edit current Editor: Enable Dansguardian Enter your description here... FAQ Filebrowser: Filter port: Filter settings FireHOL FireHol Firefox proxy settings unlocked Firehol running at startup Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language: Load selected configuration Lock Firefox Proxy Settings Logs Logs unlocked Manual... Minimal Minimal Filtering Minimal Filtering: Misc. Settings Moderate Moderate Filtering Moderate Filtering: Naughtyness Limit Netbrowser: OK Open in external browser Open log directory Open main configuration files... Other Port settings Preset configurations Process Complete. Dansguardian has now been completely disabled. You should restart any web browsers that you have running. Process Complete. Dansguardian has now been completely enabled. You should restart any web browsers that you have running. Process Complete. You should now be able to adjust your Firefox proxy settings. You must restart Firefox for the changes to take effect. Process Complete. Your Firefox proxy settings are now locked. You must restart Firefox for the changes to take effect. Program permissions Protection Proxy port: Refresh Reload Reload current ports Report a bug... Save Save custom apps settings Services Setting description Sites Not Filtered Some description of the settings. :) Strict Strict (Default) Filtering Strict (Default) Filtering: Sudo frontend: Test access denied page This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy Tinyproxy running at startup Totalcontrol files Troubleshooting URLs Not Filtered Unlock Firefox Proxy Settings User: View &console View in external browser View with text editor WPA interfaces unlocked Web Content Control Websites tinyproxy+firehol+firefox config files Project-Id-Version: webcontentcontrol
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-06-17 21:04+0000
Last-Translator: zacharyjos <Unknown>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-09-25 23:12+0000
X-Generator: Launchpad (build Unknown)
 &O programie... &Eksportuj... &Plik &Pomoc &Importuj... &Opcje &Wyjdź * Umiarkowane blokowanie obrazków
* Umiarkowane blokowanie rozszerzeń plików
* Umiarkowane blokowanie typów MIME
* Ustawiony Limit Niegrzeczności dla starszych dzieci * Nie blokuje obrazków
* Nie blokuje rozszerzeń plików
* Nie blokuje typów MIME
* Limit Niegrzeczności ustawiony dla młodzieży * Dokładne blokowanie obrazków
* Dokładne blokowanie rozszerzeń plików
* Dokładne blokowanie typów MIME
* Ustawiony Limit Niegrzeczności dla małych dzieci ... => AdBlocker Tryb administratora (spyta mnie o hasło) Ustawienia zaawansowane Zezwolone wyrażenia/frazy Dopuszczone grupy Zastosuj Zastosuj wszystkie ustawienia Zastosuj ustawienia portów Automatycznie ustaw porty. Zablokowane Strony Blokowane adresy URL Zablokowane grupy Czarnalista (może zabrać sporo miejsca) Blokowane rozszerzena plików Zablokowane Adresy IP Blokowane typy plików (MIME) Blokowane Zwroty/Frazy Porzuć Sprawdź obecne Sprawdź opcjonalne pozycje do wyeksportowania tutaj: Konfiguruj CZARNĄLISTĘ (blacklist) Konfiguruj BIAŁĄ LISTĘ (whitelist) Dostosuj Dostosuj ustawienia aplikacji Użytkownika: DansGuardian Domyśle ustawienia DansGuardiana Uruchamiaj DansGuardiana przy starcie systemu Opis Wyłącz DansGuardiana Wyłącz odświeżanie statusu/stanu Edytuj obecne Edytor: Włącz DansGuardiana Podaj swój opis tutaj... FAQ Przeglądarka Plików: Port nasłuchujący: Ustawienia filtru FireHol FireHol Odblokuj ustawienia sieci przeglądarki Firefox Uruchamiaj Firehola na starcie Gnome (gksudo + gedit) Gnome czy KDE? Gnome czy KDE?: Pomóż w &rozwijaniu Pomóż w &tłumaczeniu Pomóż w &testowaniu Nie Filtrowane adresy IP Filtrowanie Obrazków KDE (kdesudo + kate) Język interfejsu Język: Załaduj wybraną konfigurację Zablokuj możliwość wyboru serwera proxy w ustawieniach Firefoksa Zdarzenia (logi) Zdarzenia (logi) zablokowane Manual... Minimalne Minimalne Filtrowanie Minimalne Filtrowanie: Rozmaite Ustawienia Umiarkowany Umiarkowane filtrowanie Umiarkowane Filtrowanie: Limit Niegrzeczności Przeglądarka Internetowa: OK Otwórz w zewnętrznej przeglądarce Otwórz katalog zdarzeń(logów) Otwórz głowny plik konfiguracyjny Pozostałe Ustawienia portu Bieżąca konfiguracja Proces Zakończony: DansGuardian został właśnie całkowicie wyłączony. Powinieneś zrestartować (wyłączyć i włączyć ponownie) wszystkie przeglądarki internetowe (Firefox, Opera, IE), które są włączone. Proces Ukończony: DansGuardian został właśnie uruchomiony. Powinieneś zrestartować (wyłączyć i włączyć) wszystkie przeglądarki internetowe, które są uruchomione. Proces Zakończony: Powinieneś teraz mieć możliwość wyboru serwera proxy w Firefoksie. Musisz zrestartować Firefoksa aby zmiany odniosły efekt. Proces Zakończony: Możliwość zmian serwerów proxy w Twoim Firefoksie została zablokowana. Musisz teraz zrestartować Firefoksa, aby zmiany odniosły efekt. Prawa dostępu programu Ochrona Port pośrednika proxy (squid, tinyproxy) Odśwież Wczytaj ponownie Przeładuj aktualne porty Powiadom o błędzie... Zapisz Zapisz ustawienia urzytkownika Usługi Opis Ustawień Nie Filtrowane Strony Jakiś opis ustawień ;) Precyzyjny Dokładnie (Domyślne) Filtrowanie Dokładne (Domyślne) Filtrowanie: Graficzna nakładka dla sudo Przetestuj stronę Dostęp Zablokowany To jest GUI (Graficzny Interfejs Użytkownika) do prostej konfiguracji, uruchamiania i zatrzymywania filtracji zawartości stron www, oparty na DansGuardianie, TinyProxy i FireHolu. TinyProxy Uruchamiaj Tinyproxy przy starcie systemu Sumaryczne zarządzanie plikami Rozwiązywanie problemów Nie filtrowane adresy URL Odblokuj możliwość wyboru serwera proxy w Firefoksie Użytkownik: Pokaż &konsole Podglądnij w zewnętrznej przeglądarce Podglądnij w edytorze tekstu Odblokuj interfejs WPA Filtr Rodzinny Strony WWW Pliki konfiguracyjne tinyproxy+firehol+firefox 