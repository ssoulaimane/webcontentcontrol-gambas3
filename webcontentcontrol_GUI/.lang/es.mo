��    }        �   �      �
  	   �
  
   �
     �
     �
  
   �
     �
     �
  �   �
  u   N  �   �     H     L  	   O  )   Y     �     �     �     �     �     �     �     �     �     	  $        <     T     `     s     �     �  $   �     �     �     �     �                    .     N     Z     o     �     �     �     �     �     �     �     �     �               ,     G     ^     l     {     �     �     �     �     �     �  	   �     �          7     <  	   J     T     \     n     �     �     �     �     �     �     �     �     �           .     4     B  {   X  z   �  �   O  v   �     O  
   c     n     z     �     �     �     �     �     �     �     �  $   �     "     )     D     `     o  {   �  	             *     =     M     _     }     �     �     �     �     �     �  &   �  �       �     �     �     �     �  	   	       �     �   �  �   u     -     1  	   4  ;   >     z     �     �     �     �  $   �  %         &     8     H  *   ]  !   �     �     �     �     �     �  7   �     6     O     i  &   w     �     �     �  +   �            &   (     O     `     h      }     �     �     �     �     �     �  3   �  &   #     J     a     p     �     �     �     �     �     �     �       %   
  /   0  	   `     j  	   �     �     �     �     �     �     �     �      �           .      6      T   1   t      �      �      �   �   �   }   _!  �   �!  �   s"     �"     #     #  
   1#     <#     E#     c#     x#  2   �#  	   �#     �#     �#  %   �#     $  "   $  #   ;$     _$  #   t$  �   �$  	   A%  (   K%     t%     �%     �%  2   �%     �%     �%      &     &     7&     T&  
   n&  7   y&         ^   !          D   y         H              [   {   P   m      .          W   R                              :      v              q   c   &   j   g   d   `             e      ?   F   a   o   J   $   %   b      G      h       @      Q   u   k          (   6   L       t       E   A   _   M   '   Y          I   f   l   0      x   \   <   U           p       n   T   2   >   ]   Z   "       3   4       	       ;       N           5   7       8   
   /       9       -       |             +       X          ,   i   1       #   S   K   }   r   w       =      )       s      C   O       B               *       z   V                   &About... &Export... &File &Help &Import... &Options &Quit * Moderate Image Filtering

* Moderate File Extension Blocking

* Moderate MIME Type Blocking

* Naughtyness Limit set for older children * No Image Filtering

* No File Extension Blocking

* No MIME Type Blocking

* Naughtyness Limit set for young adults * Strict Image Filtering

* Strict File Extension Blocking

* Strict MIME Type Blocking

* Naughtyness Limit set for young children ... => AdBlocker Admin mode (i.e. ask me for the password) Advanced settings Allowed Phrases Allowed groups Apply Apply all settings Apply port settings Auto set ports Banned Sites Banned URLs Banned groups Blacklists (can take a lot of space) Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Check optional items to export here: Configure BLACKLISTS Configure WHITELISTS Custom Custom apps settings Custom: DansGuardian DansGuardian defaults Dansguardian running at startup Description Disable Dansguardian Disable status refresh Edit current Editor: Enable Dansguardian Enter your description here... FAQ Filebrowser: Filter port: Filter settings FireHOL FireHol Firefox proxy settings unlocked Firehol running at startup Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language: Load selected configuration Lock Firefox Proxy Settings Logs Logs unlocked Manual... Minimal Minimal Filtering Minimal Filtering: Misc. Settings Moderate Moderate Filtering Moderate Filtering: Naughtyness Limit Netbrowser: OK Open in external browser Open log directory Open main configuration files... Other Port settings Preset configurations Process Complete. Dansguardian has now been completely disabled. You should restart any web browsers that you have running. Process Complete. Dansguardian has now been completely enabled. You should restart any web browsers that you have running. Process Complete. You should now be able to adjust your Firefox proxy settings. You must restart Firefox for the changes to take effect. Process Complete. Your Firefox proxy settings are now locked. You must restart Firefox for the changes to take effect. Program permissions Protection Proxy port: Refresh Reload Reload current ports Report a bug... Save Save custom apps settings Services Setting description Sites Not Filtered Some description of the settings. :) Strict Strict (Default) Filtering Strict (Default) Filtering: Sudo frontend: Test access denied page This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy Tinyproxy running at startup Totalcontrol files Troubleshooting URLs Not Filtered Unlock Firefox Proxy Settings User: View &console View in external browser View with text editor WPA interfaces unlocked Web Content Control Websites tinyproxy+firehol+firefox config files Project-Id-Version: webcontentcontrol
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-05-18 17:14+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-09-25 23:24+0000
X-Generator: Launchpad (build Unknown)
 &Acerca de... &Exportar... &Archivo A&yuda &Importar... &Opciones &Salir * Filtrado moderado de imágenes

* Bloqueo moderado de extensiones de archivos

* Bloqueo moderado de tipos MIME

* Ajustado el nivel de contenidos inapropiados para niños mayores * Sin filtrado de imágenes

* Sin bloqueo de extensiones de archivos

* Sin bloqueo de tipos MIME

* Ajustado el nivel de contenidos inapropiados para adolescentes * Filtrado estricto de imágenes

* Bloqueo estricto de extensiones de archivos

* Bloqueo estricto de tipos MIME

* Ajustado el nivel de contenidos inapropiados para niños pequeños ... => AdBlocker Modo de administración (es decir, me piden la contraseña) Configuraciones  avanzadas Frases permitidas Grupos permitidos Aplicar Aplicar todos los ajustes Aplicar la configuración de puertos Configurar el puerto automáticamente Sitios prohibidos URLs Prohibidas Grupos no permitidos Listas negras (puede ocupar mucho espacio) Extensiones de archivo bloqueadas IPs bloqueadas Tipos MIME bloqueados Frases bloqueadas Cancelar Comprobar la actual Compruebe los elementos opcionales para exportar aquí: Configurar LISTAS NEGRAS Configurar LISTAS BLANCAS Personalizado Ajustes personalizados de aplicaciones Personalizado: DansGuardian Predefinidos de Dansguardian Ejecutar Dansguardian al iniciar el sistema Descripción Desactivar Dansguardian Desactivar la actualización de estado Editar la actual Editor: Activar Dansguardian Escriba aquí su descripción... FAQ Navegador de archivos: Filtrar el puerto: Ajustes de filtros FireHOL FireHol Desbloqueada la configuración del proxy en Firefox Ejecutar Firehol al iniciar el sistema Gnome (gksudo + gedit) ¿Gnome o KDE? ¿Gnome o KDE?: Ayuda &código... Ayuda &traducción... Ayuda prueba &final... IPs no filtradas Filtrado de imágenes KDE (kdesudo + kate) Idioma Idioma: Cargar la configuración seleccionada Configuración del bloqueo del proxy de Firefox Registros Registros desbloqueados Manual... Mínimo Filtrado mínimo Filtrado mínimo: Ajustes (miscelánea) Moderado Filtrado moderado Filtrado moderado: Nivel de contenidos inapropiados Navegador de red: Aceptar Abrir en un navegador externo Abrir el directorio de registro Abrir los archivos de configuración principal... Otros Configurar el puerto Configuraciones predefinidas Proceso completado. Dansguardian ha sido desactivado totalmente. Debe reiniciar cualquier navegador web que se esté ejecutando. Proceso completado. Dansguardian ha sido activado totalmente. Debe reiniciar cualquier navegador web que se esté ejecutando. Proceso completado. Ahora debe ser capaz de ajustar su configuración de proxy de Firefox. Debe reiniciar Firefox para que los cambios surtan efecto. Proceso completado.  Los ajustes del proxy de Firefox están ahora bloqueados. Debe reiniciar Firefox para que los cambios surtan efecto. Permisos de programas Protección Puerto del proxy: Actualizar Recargar Recargar los puertos actuales Reportar un error... Guardar Guardar los ajustes personalizados de aplicaciones Servicios Descripción del ajuste Sitios no filtrados Breve descripción de los ajustes. :) Estricto Filtrado estricto (predeterminado) Filtrado estricto (predeterminado): Interfaz de «sudo» Prueba de acceso denegado a página Esta es una interfaz gráfica fácil de configurar, iniciar y detener el «control de contenidos web» basado en la configuración de DansGuardian, TinyProxy y FireHol. TinyProxy Ejecutar Tinyproxy al iniciar el sistema Control total de archivos Solución de problemas URLs no Filtrados Configuración del desbloqueo del proxy de Firefox Usuario: Ver &consola Ver en un navegador externo Ver con un editor de texto Interfaces WPA desbloqueadas Control de contenidos web Sitios web Archivos de configuración de tinyproxy+firehol+firefox 