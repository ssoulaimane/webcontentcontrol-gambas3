��    ~        �   �      �
  	   �
  
   �
     �
     �
  
   �
     �
     �
  �   �
  u   v  �   �     p     t  	   w  )   �     �     �     �     �     �     �     	          %     1  $   ?     d     |     �     �     �     �  $   �     �     �               +     3     @     V     v     �     �     �     �     �     �     �     �               $     ,     4     T     o     �     �     �     �     �     �     �     �       
     	   (     2     N     j     o  	   }     �     �     �     �     �     �     �     �                    -      @     a     g     u  {   �  z     �   �  v        �  
   �     �     �     �     �     �     �     �           	       $   0     U     \     w     �     �  {   �  	   6     @     ]     p     �     �     �     �     �     �     �            &   (  �  O     �  	   �            	                �   (  �   �  �   |     !     %     (  8   @     y     �     �  	   �     �  "   �  #   �     #     0     E  @   T     �     �     �     �     �     �  %        9     S     l  (   z     �     �  '   �  !   �     
       %   /     U     ^     g     |     �     �     �     �     �     �  (   �          :     Q     `     p     �  &   �     �     �     �                 &   #  (   J     s     �     �     �     �     �     �     �                 &      :      O   #   W      {   '   �      �      �      �   k   �   o   W!  �   �!  �   k"     �"  
   #     #  
   #  	   (#     2#     N#     g#  8   s#     �#     �#     �#  &   �#     $     $     8$     W$  !   g$  �   �$  	   T%     ^%     }%  
   �%     �%  +   �%     �%     �%  #   &  "   7&     Z&  =   t&     �&  :   �&     D                       T   "   h   R   1   '      r   e               }      {   ;           Y               4   H      p   W   c   #   :      @   m   *                      $      t       ^   	   P   !   3   .   L          ,   %       +   j               K   X      [       g   U      Z   `   n   k   v       )         f   Q       s   0                    /   _   l      2   z   o                  A   >                  b   V         i   =   x   O       
       7   E       ~   y   S   6       a       G   M   -   N       9   F   J       &           q      I   B       d      w   u           5   8       <   \   C   |   ]       ?                 (           &About... &Export... &File &Help &Import... &Options &Quit * Moderate Image Filtering

* Moderate File Extension Blocking

* Moderate MIME Type Blocking

* Naughtyness Limit set for older children * No Image Filtering

* No File Extension Blocking

* No MIME Type Blocking

* Naughtyness Limit set for young adults * Strict Image Filtering

* Strict File Extension Blocking

* Strict MIME Type Blocking

* Naughtyness Limit set for young children ... => AdBlocker Admin mode (i.e. ask me for the password) Advanced settings Allowed Phrases Allowed groups Apply Apply all settings Apply port settings Auto set ports Banned Sites Banned URLs Banned groups Blacklists (can take a lot of space) Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Check optional items to export here: Configure BLACKLISTS Configure WHITELISTS Custom Custom apps settings Custom: DansGuardian DansGuardian defaults Dansguardian running at startup Description Disable Dansguardian Disable status refresh Edit current Editor: Enable Dansguardian Enter your description here... FAQ Filebrowser: Filter port: Filter settings FireHOL FireHol Firefox proxy settings unlocked Firehol running at startup Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language : Language: Load selected configuration Lock Firefox Proxy Settings Logs Logs unlocked Manual... Minimal Minimal Filtering Minimal Filtering: Misc. Settings Moderate Moderate Filtering Moderate Filtering: Naughtyness Limit Netbrowser: OK Open in external browser Open log directory Open main configuration files... Other Port settings Preset configurations Process Complete. Dansguardian has now been completely disabled. You should restart any web browsers that you have running. Process Complete. Dansguardian has now been completely enabled. You should restart any web browsers that you have running. Process Complete. You should now be able to adjust your Firefox proxy settings. You must restart Firefox for the changes to take effect. Process Complete. Your Firefox proxy settings are now locked. You must restart Firefox for the changes to take effect. Program permissions Protection Proxy port: Refresh Reload Reload current ports Report a bug... Save Save custom apps settings Services Setting description Sites Not Filtered Some description of the settings. :) Strict Strict (Default) Filtering Strict (Default) Filtering: Sudo frontend: Test access denied page This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy Tinyproxy running at startup Totalcontrol files Troubleshooting URLs Not Filtered Unlock Firefox Proxy Settings User: View &console View in external browser View with text editor WPA interfaces unlocked Web Content Control Websites tinyproxy+firehol+firefox config files Project-Id-Version: webcontentcontrol
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-06-19 21:13+0000
Last-Translator: Sangy <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-09-25 22:32+0000
X-Generator: Launchpad (build Unknown)
 &A propos... &Exporter &Menu &Aide &Importer &Options &Fermer * Filtrage modéré des images

* Blocage modéré des extensions de fichier

* Filtrage modéré des types MIME

* Filtrage du contenu configuré pour les enfant plus agés * Pas de filtrage  des images

* Pas de blocage des extensions de fichier

* Pas de filtrage des types MIME

* Filtrage du contenu configuré pour les jeunes adultes * Filtrage strict des images

* Blocage strict des extensions de fichier

* Filtrage strict des types MIME

* Filtrage du contenu configuré pour les jeunes enfants ... => Bloqueur de publicités Mode Administrateur (accès contrôlé par mot de passe) Paramètre avancés Phrases autorisées Groups autorisés Appliquer Appliquer tous les changements Appliquer les paramètres de ports Configuration automatique des ports Sites bannis Adresses URL bannies Groupes bannis Listes Noires (peut nécessiter beaucoup d'espace sur le disque) Extensions de fichier bloqués Adresses IP bloquées Types MIME bloqués Phrases bloquées Annuler Vérifier la page actuelle Vérifiez les éléments à exporter: Configurer la Liste Noire Configurer Liste Blanche Personnalisé Paramètres d'applications personalisés Personnalisés: DansGuardian Paramètres par défaut de Dansguardian Lancer Dansguardian au démarrage Description Désactiver Dansguardian Désactiver la mise à jour du statut Modifier Editeur: Activer Dansguardian Entrer votre description ici... FAQ Gestionnaire de fichiers: Port de filtrage: Paramètres de filtrage FireHOL FireHol Paramètres de proxy Firefox débloqués Lancer Firehol au démarrage Gnome (gksudo + gedit) Gnome ou KDE ? Gnome ou KDE ?: Aider à la &programmation Aider à la &traduction Aider à tester les nouvelles versions Adresses IP non-filtrées Filtrage d'images KDE (kdesudo + kate) Langue Langue : Langue: Charger la configuration selectionnée Bloquer les paramètres proxy de Firefox Historiques de connection Historiques débloqués Manuel d'utilisation Minimum Filtrage minimum Filtrage minimum: Paramètres divers Modéré Filtrage modéré Filtrage modéré: Filtrage du contenu Navigateur Internet: Valider Afficher dans un navigateur externe Ouvrir l'historique Ouvrir les fichiers de configuration... Autre Paramètres des ports Paramètres par défaut Processus terminé. Dansguardian est maintenant désactivé. Tout navigateur ouvert doit être redémarré. Processus terminé. Dansguardian est maintenant activé. Tout navigateur Internet ouvert doit être redemarré. Processus terminé. Vous pouvez dès à présent modifier les paramètres proxy de Firefox. Vous devez redémarrer Firefox pour que les changements prennent effet. Processus terminé. Vos paramètres proxy de Firefox sont bloqués. Vous devez redémarrer Firefox pour que les changements prennent effet. Permissions Protection Port de proxy: Rafraichir Recharger Recharger les ports actuels Signaler un problème... Enregistrer Sauvegarder les paramètres d'application personnalisés Services Description de parametrage Sites Internet non-filtrés Courte description des paramètres. :) Strict Filtrage strict (par défaut) Filtrage strict (par défaut): Interface Sudo: Test "accès à la page interdit" Ce programme est une interface graphique permettant de configurer, lancer et arrêter facilement un programme de filtrage du contenu Internet basé sur les programmes DansGuardian, TinyProxy et FireHol. TinyProxy Lancer Tinyproxy au démarrage Fichiers de contrôle global Dépannage Adresses Internet non-filtrées Débloquer les paramètres proxy de Firefox Utilisateur: Afficher la &Console Afficher dans un navigateur externe Afficher dans un éditeur de texte Interface WPA débloquée Web Content Control -Logiciel de filtrage du contenu Internet Sites Internet Fichiers de configuration de Tinyproxy, Firehol et Firefox 