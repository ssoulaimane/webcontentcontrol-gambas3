��    �          �      �  	   �  
   �            
             &  �   ,  u   �  �   ,     �     �     �  	   �  )   �               -     <     B     U     i     q     �     �     �  	   �  $   �     �     �     �            A   $     f  $   t  	   �     �     �     �     �     �     �     �  #        /     O     [     p     �     �     �     �     �     �  !   �                    ,     4     <  
   H     S     q     �     �     �     �     �     �               (     B     S     c     x       
   �  	   �     �     �     �     �     �  	   �     �               '     5     >     Q     e     w     �     �     �  	   �     �     �     �      �                 {   ,  z   �  �   #  v   �     #  
   7     B     Q     _     k     s     z     �  :   �     �     �     �            $   )  *   N     y     �     �     �     �  	   �  	   �  {   �  	   n     x     �      �     �     �     �     �          $     *     8     Q     g     }     �     �  	   �     �     �     �     �     �               0     ?     M     b     t     �     �     �     �     �     �               -     ?     P     a     n     z     �     �  
   �     �     �  &   �     �     �  �      
   �      �      !     !     %!     =!     O!  �   [!  �   "  �   �"     {#     #  7   �#      �#  ;   �#     $  1   3$  (   e$  
   �$  &   �$  -   �$     �$  0   �$     0%  ,   P%  #   }%     �%  L   �%  ,   &      9&      Z&  ,   {&  
   �&  X   �&     '  J   '  	   f'     p'     y'  	   �'     �'     �'     �'  .   �'  ;   	(  4   E(  
   z(     �(  3   �(     �(     �(     �(     )     0)     =)  0   \)     �)     �)     �)     �)     �)     �)     *  .   *  5   I*  6   *  /   �*     �*     �*     +      +  %   ;+  %   a+  8   �+  3   �+     �+     
,     ,  
   &,     1,     =,  
   I,  &   T,  *   {,     �,     �,     �,  
   �,     �,     -     -  
   7-     B-     Z-      s-     �-     �-  
   �-      �-     �-     �-  !   .     (.  9   C.     }.  "   �.     �.  �   �.  �   n/  �   0  �   �0     X1     v1     �1     �1  !   �1  
   �1  
   �1  0   �1     &2  I   F2     �2  (   �2     �2     �2  2   �2  #   3  S   <3     �3  *   �3  +   �3     �3      4  	   -4  	   74  �   A4  	   5     5     25  8   G5  1   �5  $   �5  /   �5  ?   6  5   G6     }6     �6  !   �6  !   �6     �6  "    7      #7     D7     S7     q7     x7     �7     �7     �7     �7     �7     �7     �7  6   8     <8     N8     `8     t8     �8     �8     �8     �8     �8     �8     9     9     *9     ;9     H9     T9     e9     v9  
   �9  
   �9  1   �9  5   �9      :     :     �      .   m             �   (      �       ;       �          �   �   �   �       8       �   o   l   /       x   Q   _       �       �   f       �   �   �   Y   E              1   %   j   {       �   F   L               =   �           g                  �       t   #       k   �   b   �       G   3      h   �           )      �   �   J       '       �           d                   n   `       �   +   �   e   U       <   a      �      �   �   �       I   c       >   M   6   $                  �   �   !   �         P   O      B          X   �   �   @   i       �   y       4   C   �   �   �   �          �      �   ~   \   u      T          	   "   [       �       7   D   5       �   V          K       *   �      �      �   �       ^       R   s   :   Z   w   �       z       &       �       9   �   q      �           A   �   �       H   0   |   �      �             ,             �   }   �       �       �   �       p   v   W   2   �           S           
   -   r   �   ?   ]   N    &About... &Export... &File &Help &Import... &Options &Quit * Moderate Image Filtering

* Moderate File Extension Blocking

* Moderate MIME Type Blocking

* Naughtyness Limit set for older children * No Image Filtering

* No File Extension Blocking

* No MIME Type Blocking

* Naughtyness Limit set for young adults * Strict Image Filtering

* Strict File Extension Blocking

* Strict MIME Type Blocking

* Naughtyness Limit set for young children ... => Access specific files directly: AdBlocker Admin mode (i.e. ask me for the password) Advanced settings Allowed Phrases Allowed groups Apply Apply all settings Apply port settings Authors Auto set ports Banned Sites Banned URLs Banned groups Blacklist Blacklists (can take a lot of space) Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Changes will take effect the next time you start the application. Check current Check optional items to export here: ComboBox1 Custom Custom apps settings Custom: DansGuardian DansGuardian Off DansGuardian On DansGuardian defaults Dansguardian not running at startup Dansguardian running at startup Description Disable Dansguardian Disable status refresh Edit current Editor: Enable Dansguardian Enter your description here... Experimental FAQ Failed to fix the runtime errors. Filebrowser: Filter port: Filter settings FireHOL FireHol FireHol Off FireHol On Firefox proxy settings locked Firefox proxy settings unlocked Firehol not running at startup Firehol running at startup General Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Label1 Language Language : Language: License Load selected configuration Lock Firefox Proxy Settings Logs Logs unlocked Manual... Minimal Minimal Filtering Minimal Filtering: Miscellaneous Moderate Moderate Filtering Moderate Filtering: Naughtyness Limit Netbrowser: No OK Open all files Open file Open file location Open in external browser Open log directory Open main configuration files... Other Port settings Preset configurations Process Complete. Dansguardian has now been completely disabled. You should restart any web browsers that you have running. Process Complete. Dansguardian has now been completely enabled. You should restart any web browsers that you have running. Process Complete. You should now be able to adjust your Firefox proxy settings. You must restart Firefox for the changes to take effect. Process Complete. Your Firefox proxy settings are now locked. You must restart Firefox for the changes to take effect. Program permissions Protection Protection Off Protection On Proxy port: Refresh Reload Reload current ports Report a bug... Runtime errors were detected.

Would you like to fix them? Save Save custom apps settings Services Setting description Sites Not Filtered Some description of the settings. :) Sorry, you can only do this in admin mode. Strict Strict (Default) Filtering Strict (Default) Filtering: Sudo frontend: Test access denied page TextArea1 TextArea2 This is a GUI to easily configure, start and stop a Web content control setup based on DansGuardian, TinyProxy and FireHol. TinyProxy TinyProxy Off TinyProxy On Tinyproxy not running at startup Tinyproxy running at startup Totalcontrol files Troubleshooting URLs Not Filtered Unlock Firefox Proxy Settings User: View &console View in external browser View with text editor WPA interfaces locked WPA interfaces unlocked Web Content Control Websites Whitelist Yes bannedextensionlist bannediplist bannedmimetypelist bannedphraselist bannedregexpheaderlist bannedregexpurllist bannedsitelist bannedurllist choose_categories.py contentregexplist dansguardian.conf dansguardianf1.conf exceptionextensionlist exceptionfilesitelist exceptionfileurllist exceptioniplist exceptionmimetypelist exceptionphraselist exceptionregexpurllist exceptionsitelist exceptionurllist filtergroupslist greysitelist greyurllist headerregexplist logregexpurllist logsitelist logurllist pics superbanner.py tinyproxy+firehol+firefox config files urlregexplist weightedphraselist Project-Id-Version: webcontentcontrol_GUI 3.7.1
PO-Revision-Date: 2015-06-01 06W26 UTC
Last-Translator: admin <admin@PC>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &حول... الت&صدير ... &ملف مسا&عدة الا&ستيراد ... ال&خيارات إن&هاء * تصفية معتدلة للصور 

 * تصفية معتدلة لملحقات الملفات 

 * تصفية معتدلة لأنوع MIME 

 * ذروة تحمل البذاءة متوسطة * لا تصفية للصور 

 * لا تصفية لملحقات الملفات 

 * لا تصفية لأنوع MIME 

 * ذروة تحمل البذاءة عالية * تصفية صارمة للصور 

 * تصفية صارمة لملحقات الملفات 

 * تصفية صارمة لأنوع MIME 

 * ذروة تحمل البذاءة منخفضة ... => الوصول إلى ملفات محددة مباشرة: حجب الإعلانات فقط وضع المدير (أي: اسأل عن كلمة السر) إعدادات متقدمة الكلمات والجمل المسموح بها المجموعات المسموح لها تفعيل تفعيل كافة الإعدادات تفعيل إعدادات المنفذ (Port) المطورون إعداد المنافذ (Ports) تلقائيا المواقع المحظورة عناوين المواقع المحظورة المجموعات المحظورة القائمة السوداء القوائم السوداء (يمكن أن تشغل مساحة كبيرة) ملحقات الملفات المحظورة عناوين IP المحظورة أنواع MIME المحظورة الكلمات والجمل المحظورة إلغاء سوف يتم تفعيل التغييرات بعد إعادة تشغيل التطبيق. الحالية تحقق من العناصر الاختيارية للتصدير، هنا: ComboBox1 مخصص إعدادات مخصصة مخصص: DansGuardian Dansguardian معطّل Dansguardian مفعّل إعدادت DansGuardian الإفتراضية عدم تشغيل Dansguardian عند بدء التشغيل تشغيل Dansguardian عند بدء التشغيل الوصف تعطيل Dansguardian تعطيل تحديث الحالة التلقائي تحرير الحالية المحرر: تفعيل Dansguardian أدخل الوصف هنا ... تجريبي الأسئلة ال&شائعة فشلت محاولة إصلاح المشاكل. مدير الملفات: تصفية المنفذ (Port): إعدادات التصفية FireHOL FireHol FireHol معطّل FireHol مفعّل إعدادات بروكسي Firefox مقفلة إعدادات بروكسي Firefox غير مقفلة عدم تشغيل Firehol عند بدء التشغيل تشغيل Firehol عند بدء التشغيل عام غنوم (gksudo + gedit) غنوم أو كيدي؟ غنوم أو كيدي ؟: مساعدة في البر&مجة ... مساعدة في التر&جمة ... مساعدة في الإ&ختبار والتحرير ... عناوبن iP التي لن تتم تصفيتها تصفية الصور KDE (kdesudo + kate) Label1 اللغة اللغة: اللغة: ترخيص تحميل الإعداد المحدد قفل إعدادات بروكسي Firefox السجلات سجلات غير مقفلة الدليل ... أساسي تصفية أساسية تصفية أساسية: إعدادات أخرى معتدل تصفية معتدلة تصفية معتدلة: ذروة تحمل البذاءة المتصفح: لا موافق إفتح جميع الملفات إفتح الملف إفتح موقع الملف فتح في متصفح خارجي فتح دليل السجل فتح ملفات الإعدادات الرئيسية ... أخرى إعدادات المنفذ (Port) إعدادات مسبقة تمت العملية، بحمد الله. وقد تم الآن تعطيل Dansguardian. يجب عليك إعادة تشغيل متصفحات الويب المشتغلة. تمت العملية، بحمد الله. وقد تم الآن تفعيل Dansguardian. يجب عليك إعادة تشغيل متصفحات الويب المشتغلة. تمت العملية، بحمد الله. وقد تم الآن إلغاء قفل إعدادات بوكسي  Firefox. يجب عليك إعادة تشغيل Firefox. تمت العملية، بحمد الله. وقد تم الآن قفل إعدادات بوكسي  Firefox. يجب عليك إعادة تشغيل Firefox. أذونات البرنامج الحماية الحماية مفعّلة الحماية معطّلة منفذ (Port) البروكسي: تحديث تحديث تحديث المنافذ (Ports) الحالية الإبلا&غ عن خطأ ... تم اكتشاف بعض الأخطاء. 

هل تريد إصلاحها؟ حفظ حفظ الإعدادات المخصصة الخدمات وصف الإعداد المواقع التي لن تتم تصفيتها الإعدادات المبدئية عفوا، لا تستطيع القيام بهذا إلا في وضع المدير. صارم تصفية صارمة (الإفتراضي) تصفية صارمة (الإفتراضي): واجهة سودو (sudo): اختبار صفحة الحظر TextArea1 TextArea2 هذا البرنامج عبارة عن واجهة رسومية هدفها تيسير تسيير نظام رقابة لمحتوى الويب يعتمد على البرامج DansGuardian وTinyProxy وFireHol. TinyProxy TinyProxy معطّل TinyProxy مفعّل عدم تشغيل Tinyproxy عند بدء التشغيل تشغيل Tinyproxy عند بدء التشغيل ملفات التحكم الشامل استكشاف الأخ&طاء وإصلاحها عناوين المواقع التي لن تتم تصفيتها إلغاء قفل إعدادات بروكسي Firefox العضو: عرض ال&طرفية فتح في متصفح خارجي عرض في محرر النصوص واجهات WPA مقفلة واجهات WPA غير مقفلة مراقب محتوى الويب المواقع القائمة البيضاء نعم bannedextensionlist bannediplist bannedmimetypelist bannedphraselist bannedregexpheaderlist bannedregexpurllist bannedsitelist bannedurllist choose_categories.py (اختيار التصنيفات) contentregexplist dansguardian.conf dansguardianf1.conf exceptionextensionlist exceptionfilesitelist exceptionfileurllist exceptioniplist exceptionmimetypelist exceptionphraselist exceptionregexpurllist exceptionsitelist exceptionurllist filtergroupslist greysitelist greyurllist headerregexplist logregexpurllist logsitelist logurllist الصور superbanner.py (إضافة مواقع للحظر) ملفات إعدادت tinyproxy + firehol + firefox urlregexplist weightedphraselist 