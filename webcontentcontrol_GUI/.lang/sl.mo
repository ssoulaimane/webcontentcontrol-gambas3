��    ;      �  O   �        	   	  
             $  
   *     5     >     D  )   G     q     �     �     �     �     �     �     �     �     �               $     9     F     \     h          �     �     �     �     �     �     �     �     �     	     #     4     D     Y  	   b     l  	   q     {     �     �     �     �     �     �     �  	   �     �                    3  �  I     2
  
   A
  	   L
     V
  	   ^
     h
     t
     }
  $   �
     �
     �
     �
     �
     �
                5     S  	   f     p  )   �  '   �     �     �               0     N     d     �     �     �     �     �     �     �     �          "     3     H     N     U     g     �     �  "   �     �     �     �     �     �  	             $  
   :     E     c            "      9             .   +   	       8          6                                    $            !             1       ,      :       %                          &   4   #   0      )       7          5       -   '            
       (       3   2       *   ;   /                &About... &Export... &File &Help &Import... &Options &Quit => Admin mode (i.e. ask me for the password) Allowed Phrases Apply Apply all settings Banned Sites Banned URLs Blocked File Extensions Blocked IPs Blocked MIME Types Blocked Phrases Cancel Check current Configure BLACKLISTS Configure WHITELISTS DansGuardian DansGuardian defaults Description Disable status refresh Edit current Enable Dansguardian FAQ FireHOL FireHol Gnome (gksudo + gedit) Gnome or KDE? Gnome or KDE?: Help &code... Help &translate... Help testing&releasing... IPs Not Filtered Image Filtering KDE (kdesudo + kate) Language Language: Logs Manual... Misc. Settings OK Open log directory Refresh Report a bug... Save Sites Not Filtered Test access denied page TinyProxy Troubleshooting URLs Not Filtered User: View in external browser View with text editor Project-Id-Version: webcontentcontrol 1.2.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2002-11-01 04:27+0100
PO-Revision-Date: 2009-07-14 13:34+0100
Last-Translator: Matej Kovacic <matej.kovacic@owca.info>
Language-Team: Slovenian <matej.kovacic@owca.info>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-07-14 10:52+0000
X-Generator: Launchpad (build Unknown)
X-Poedit-Language: Slovenian
X-Poedit-Country: SLOVENIA
 &O programu... &Izvozi... &Datoteka &Pomoč &Uvozi... &Nastavitve &Končaj => Skrbniški način (vpraša za geslo) Dovoljene besede Uporabi Uveljavi vse spremembe Nedovoljene domene Nedovoljeni URL naslovi Nedovoljene končnice datotek Nedovoljeni IP naslovi Nedovoljeni MIME tipi datotek Nedovoljene besede Prekliči Prikaz trenutnega obvestila Nastavitve seznama prepovedanih elementov Nastavitve seznama dovoljenih elementov DansGuardian DansGuardian privzete vrednosti Opis Onemogoči osveževanje statusa Urejanje trenutnega obvestila Omogoči Dansguardian Pogosta vprašanja in odgovori FireHOL FireHol Gnome (gksudo + gedit) Gnome ali KDE? Gnome ali KDE?: Pomagaj p&rogramirati... Pomagaj &prevesti... Pomagaj &testirati... Dovoljeni IP naslovi Filtriranje slik KDE (kdesudo + kate) Jezik Jezik: Dnevniški zapisi Priročnik za uporabo... Ostale nastavitve OK Odpri imenik z dnevniškimi zapisi Osveži Pošlji sporočilo o napaki... Shrani Dovoljene domene Test obvestila TinyProxy Odpravljanje težav Dovoljeni URL naslovi Uporabnik: Preglej v zunanjem brskalniku Preglej v urejevalniku besedila 