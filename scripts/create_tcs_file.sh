#!/bin/bash
set -e -u

source yourpath/import_export_functions.sh

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` file"
        exit 1
fi

TC_PATH=yourpath
TC_STATUS=$TC_PATH/tc_status
EXT=tcs
TC_BANNED=$TC_PATH/tc_banned
TC_ALLOWED=$TC_PATH/tc_allowed
TC_DESCRIPTION=$TC_PATH/tc_description

#make sure setting files end with the correct extension
SETTING_NAME=$1
SETTING_ARCHIVE=$(add_extension $SETTING_NAME $EXT)

if [ ! -e $TC_DESCRIPTION ]; then echo "ERROR: $TC_DESCRIPTION missing!" && exit -1; fi;
if [ ! -e $TC_BANNED ]; then echo "ERROR: $TC_BANNED missing!" && exit -1; fi;
if [ ! -e $TC_ALLOWED ]; then echo "ERROR: $TC_ALLOWED missing!" && exit -1; fi;

echo "Saving current settings as $SETTING_ARCHIVE"
tar -Pczvf $SETTING_ARCHIVE $TC_DESCRIPTION $TC_BANNED $TC_ALLOWED /etc/dansguardian/ --exclude=lists/blacklists
echo "Saved current settings as $SETTING_ARCHIVE"
