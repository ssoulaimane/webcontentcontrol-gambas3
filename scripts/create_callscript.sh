#!/bin/bash

GAMBASDIR=webcontentcontrol_GUI
BINARY=webcontentcontrol

if [ $# -ne 1 ]
then
        echo "usage : $0 <location of $GAMBASDIR/$GAMBASDIR.gambas>"
        exit 0
fi

cat >./$BINARY <<EOF
#!/bin/bash
cd $1
$1/$GAMBASDIR/$GAMBASDIR.gambas
EOF
chmod 755 ./$BINARY
