#!/bin/bash

# TODO: add support to more startup processes as needed

############# detect startup process #############
SYSTEMD=false
SYSV=false
UPSTART=false

initproc=`ps -o comm= 1`

if [ "`basename "$initproc"`" = init ]
then
    initproc_loc=`which "$initproc"`
    initproc=`readlink "$initproc_loc"`
fi

if [ "`basename "$initproc"`" = systemd ]
then
    SYSTEMD=true
elif [ "`basename "$initproc"`" = upstart ]
then
    UPSTART=true
else
    SYSV=true
fi
##################################################

usage()
{
    echo "This script is an abstraction to handle services in different platforms"
    echo "======================================================================="
    echo "usage :"
    echo "`basename $0` action service"
    echo "    action  : one of the following"
    echo "              {status,start,stop,restart,enable,disable,is-enabled}"
    echo "    service : the service name"
    echo "======================================================================="
    exit 1
}

action=$1
service=$2

# check arguments
if [ $# -ne 2 ] || \
   [ "$action" != status     ] && [ "$action" != start   ] && \
   [ "$action" != stop       ] && [ "$action" != restart ] && \
   [ "$action" != enable     ] && [ "$action" != disable ] &&
   [ "$action" != is-enabled ] || \
   [ -z $service ]
then
    usage
fi

########## systemd ###########
if $SYSTEMD
then
    systemctl $action $service
########## upstart ###########
## elif $UPSTART
## then
##     # ...
############ sysv ############
else
    if [ $action = enable ]
    then
        update-rc.d $service defaults
    elif [ $action = disable ]
    then
        update-rc.d -f $service remove
    elif [ $action = is-enabled ]
    then
        if find /etc/rc* | grep -E "/etc/rc[0-9S].d/[SK]?[0-9]{2}$service" > /dev/null
        then
            echo enabled
            true   # update error code
        else
            echo disabled
            false  # update error code
        fi
    else
        /etc/init.d/$service $action
    fi
fi
##############################

exit $?  # propagate error code
