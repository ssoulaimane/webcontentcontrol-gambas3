#!/bin/bash
set -e -u

source yourpath/import_export_functions.sh

logout_necessary=false

#what we want:
#/etc/rc.local
# TC_load.sh --atboot 6 7 fun
# TC_load.sh --atboot 1 5 work
# 
# #crontab
# * * * * 6-7 TC_load.sh --load fun
# * * * * 1-5 TC_load.sh --load work

#check if inside time window

# Check if all parameters are present
# If no, exit
if [ $# -ne 2 ]
then
        echo "usage :"
        echo "`basename $0` user file"
        exit 0
fi

TC_PATH=yourpath
TC_STATUS=$TC_PATH/tc_status
EXT=tcs
TC_BANNED=$TC_PATH/tc_banned
TC_ALLOWED=$TC_PATH/tc_allowed
TC_DESCRIPTION=$TC_PATH/tc_description
USERNAME=$1
NEW_SETTING=$2

#check that status file exists and is not empty
if [ ! -s $TC_STATUS ]
then
    echo "ERROR: $TC_STATUS does not exist or is empty."
    exit 1
fi

#make sure setting files end with the correct extension
CURRENT_SETTING=`cat $TC_STATUS`
CURRENT_ARCHIVE=$(add_extension $CURRENT_SETTING $EXT)
NEW_ARCHIVE=$(add_extension $NEW_SETTING $EXT)

#check if those settings aren't already loaded
if [ $CURRENT_ARCHIVE = $NEW_ARCHIVE ]
then
    echo "$NEW_ARCHIVE already loaded."
    exit 0
fi

#if not, load them
echo "$NEW_ARCHIVE not loaded."

#save current settings (in case restrictions have been added ;) )
$TC_PATH/create_tcs_file.sh $CURRENT_ARCHIVE

#load TC settings
echo "Loading settings from $NEW_ARCHIVE..."
tar -Pxzvf $NEW_ARCHIVE >/dev/null
echo "Loaded settings from $NEW_ARCHIVE."

ProcessAllow()
{
    FILE=$1
    TMP=$(mktemp)
    cp -v $FILE $TMP
    sed -i '/^$/ d' $TMP
    sed -i '/^#/ d' $TMP
    nlines=$(wc -l $TMP | awk '{print $1}')
    echo nlines=$nlines
    echo "=============="
    cat $TMP
    echo "=============="
    if [ $nlines -ne 0 ]
    then
	echo "Processing groups from $FILE"
	#add user to allowed groups
	cat $TMP | xargs -n1 $TC_PATH/add_user_to_group.sh $USERNAME
	logout_necessary=true
    else
	echo "$FILE does not contain any groups"
    fi
}

ProcessBan()
{
    FILE=$1
    TMP=$(mktemp)
    cp -v $FILE $TMP
    sed -i '/^$/ d' $TMP
    sed -i '/^#/ d' $TMP
    nlines=$(wc -l $TMP | awk '{print $1}')
    echo nlines=$nlines
    echo "=============="
    cat $TMP
    echo "=============="
    if [ $nlines -ne 0 ]
    then
	echo "Processing groups from $FILE"
	#remove user from banned groups
	cat $TMP | xargs -n1 $TC_PATH/remove_user_from_group.sh $USERNAME
	logout_necessary=true
    else
	echo "$FILE does not contain any groups"
    fi
}

ProcessAllow $TC_ALLOWED
ProcessBan $TC_BANNED

#restart DG
echo "Restarting dansguardian"
yourpath/servicectl.sh restart dansguardian

#change status
echo "Setting current status to $NEW_ARCHIVE."
echo $NEW_ARCHIVE >$TC_STATUS

#TODO: logout if -l option given
if $logout_necessary
then
	echo "Logging you out so that group changes take effect. Sorry."
	$TC_PATH/logout.sh $USERNAME
fi

