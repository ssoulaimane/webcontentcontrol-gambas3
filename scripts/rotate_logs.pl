#!/usr/bin/perl -w
###############################################################################
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
# the GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
###############################################################################

###############################################################################
# Author: Robby Garrison, Vulcan Technologies
#         robby@vulcantechnologies.com
#	  
# Description:
#   Parses the Dan's Guardian access.log file for *DENIED* entries,
#   and outputs them to HTML.  Also rotates it's own output files, and
#   uses much cheapness to try to keep the access.log file rotated
#   too.
#
# To Use:
#   Change the "$log_file" and "$parser_output_dir" variables to match
#   your setup.  To run, simply execute './dg-log-parser-0.03.pl' (you may
#   need to 'chmod +x dg-log-parser-0.03.pl') and then
#   cd to the  "$parser_output_dir" and use a web browser to open the 
#   *.html file with the appropriate date name, and view your log output
#   *NOTE*  this log-parser only outputs log entries where access to a 
#   page has been denied (since these are the only entries of interest to
#   me 8-), but you can change this behavior by modifying the regex under
#   "#Begin parsing of log data".
###############################################################################
$htmllogdir=$ARGV[0];
#    ($WYear,$wMon,$WDay) = @ARGV unless defined $WYear;
if (length($htmllogdir)==0)
    {print "Usage: index.pl HTMLLOGDIR\n"; exit}
print "Indexing ${sourcedir} . index.html will be created in ${destdir} \.\n";
# exit;

use File::Copy;

# Open log file for reading
$log_file = '/var/log/dansguardian/access.log';
# $parser_output_dir = '/var/log/dansguardian/html';
$parser_output_dir = $htmllogdir;

# Make sure that the output directory for the parser *.html files exists & create if it doesn't
unless (-e $parser_output_dir && -d $parser_output_dir) {
    mkdir ("$parser_output_dir");
}

# Read current time...
($null,$minute,$hour,$day,$month,$year,$null,$null,$null)=localtime(time);

# ...and adjust as necessary
if ($hour < 12) {
    $ampm = "am";
} else {
    $ampm = "pm";
}
if ($hour == 12) {
    $hour=$hour;
} elsif ($hour == 0) {
    $hour = $hour + 12;
} elsif ($hour > 12) {
    $hour = $hour - 12;
}
if ($minute < 10) {
    $minute = "0$minute";
}
$year = $year + 1900;

$month = $month + 1;
@months = qw(January February March April May June July August September October November December);
$written_month = $months[$month - 1];

# Return the '*.html' file to the state we found it ....err, sorta.
select STDOUT;

# Check to see if "access.log" needs rotating, and do so if it does
if (-M $log_file > 0.98) {
    move ("$log_file", "$log_file.$month.$day.$year");
}

# Groom directory of files older than 14 days if more than 14 files exist
@dir_contents = glob ("$parser_output_dir");
$num_files = @dir_contents;
if ($num_files > 14) {
    foreach $file (@dir_contents) {
	if (-M $file > 14) {
	    unlink("$file");
	}
    }
}
