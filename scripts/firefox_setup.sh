#!/bin/bash
# set -x #for debugging
set -u
source yourpath/file_paths.sh

PrefName[0]='"network.proxy.type"'; PrefValue[0]=1;
PrefName[1]='"network.proxy.ftp"'; PrefValue[1]='"localhost"';
PrefName[2]='"network.proxy.ftp_port"'; PrefValue[2]=8080;
PrefName[3]='"network.proxy.gopher"'; PrefValue[3]='"localhost"';
PrefName[4]='"network.proxy.gopher_port"'; PrefValue[4]=8080;
PrefName[5]='"network.proxy.http"'; PrefValue[5]='"localhost"';
PrefName[6]='"network.proxy.http_port"'; PrefValue[6]=8080;
PrefName[7]='"network.proxy.share_proxy_settings"'; PrefValue[7]=true;
PrefName[8]='"network.proxy.socks"'; PrefValue[8]='"localhost"';
PrefName[9]='"network.proxy.socks_port"'; PrefValue[9]=8080;
PrefName[10]='"network.proxy.ssl"'; PrefValue[10]='"localhost"';
PrefName[11]='"network.proxy.ssl_port"'; PrefValue[11]=8080;
PrefName[12]='"network.proxy.no_proxies_on"'; PrefValue[12]='"localhost,127.0.0.1"';

PrefNameNum=${#PrefName[@]}
PrefValueNum=${#PrefValue[@]}

lockPref()
{
    sed -i "\$a\lockPref($1,$2);" $FF_JS >/dev/null
}

unlockPref()
{
    sed -i "/lockPref($1/d" $FF_JS >/dev/null
}

checkPref()
{
    if grep -q "^lockPref($1,$2);" $FF_JS
    then
	return 0
    else
	return 1
    fi
}

status()
{
    echo "checking $FF_JS..."
    for ((i=0;i<$PrefNameNum;i++)); do
	checkPref ${PrefName[${i}]} ${PrefValue[${i}]} || break
    done
    if [ $i -ne $PrefNameNum ]
    then
	echo "STATUS: Firefox is unlocked"
	return 0
    else
	echo "STATUS: Firefox is locked"
	return 1
    fi
}

if [ $# -ne 1 ]
then
        echo "usage : $0 <lock/unlock/status/status_for_GUI>"
        exit 0
fi

if [ $1 = 'lock' ]
then
    status
    for ((i=0;i<$PrefNameNum;i++)); do
	checkPref ${PrefName[${i}]} ${PrefValue[${i}]} || lockPref ${PrefName[${i}]} ${PrefValue[${i}]}
    done
    status
    if [ $? -eq 1 ]
    then
	echo "Locking successful"
	echo "You will need to restart Firefox for the settings to take effect." >&2
    else
	echo "Locking not successful"
    fi
fi

if [ $1 = 'unlock' ]
then
    status
    for ((i=0;i<$PrefNameNum;i++)); do
	unlockPref ${PrefName[${i}]} ${PrefValue[${i}]}
    done
    status
    if [ $? -eq 0 ]
    then
	echo "Unlocking successful"
	echo "You will need to restart Firefox for the settings to take effect." >&2
    else
	echo "Unlocking not successful"
    fi
fi

if [ $1 = 'status' ]
then
    status
    exit $?
fi

if [ $1 = 'status_for_GUI' ]
then
    status >/dev/null
    echo -n $?
fi
