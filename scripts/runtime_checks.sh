#!/bin/bash
source yourpath/file_paths.sh

######### VARS #########
USER=webcontentcontrol
GROUP=webcontentcontrol
########################

status()
{
    if [ -d $DG_LOGDIR ] && \
       [ `ls -ld $DG_LOGDIR | awk '{print $3 ":" $4}'` != "$USER:$GROUP" ]
    then
        echo "runtime checks: wrong permissions for $DG_LOGDIR"
        return 10
    elif [ -f $DG_LOGDIR/access.log ] && \
         [ `ls -ld $DG_LOGDIR/access.log | awk '{print $3 ":" $4}'` != "$USER:$GROUP" ]
    then
        echo "runtime checks: wrong permissions for $DG_LOGDIR/access.log"
        return 11
    elif [ -d $TP_LOGDIR ] && \
         [ `ls -ld $TP_LOGDIR | awk '{print $3 ":" $4}'` != "$USER:$GROUP" ]
    then
        echo "runtime checks: wrong permissions for $TP_LOGDIR"
        return 20
    elif [ -f $TP_LOGDIR/tinyproxy.log ] && \
         [ `ls -ld $TP_LOGDIR/tinyproxy.log | awk '{print $3 ":" $4}'` != "$USER:$GROUP" ]
    then
        echo "runtime checks: wrong permissions for $TP_LOGDIR/tinyproxy.log"
        return 21
    elif [ -d /var/run/tinyproxy ] && \
         [ `ls -ld /var/run/tinyproxy | awk '{print $3 ":" $4}'` != "$USER:$GROUP" ]
    then
        echo "runtime checks: wrong permissions for /var/run/tinyproxy"
        return 22
    elif ! grep -q "^daemonuser = '$USER'" $DG
    then
        echo "runtime checks: daemonuser not set to $USER"
        return 12
    elif ! grep -q "^daemongroup = '$GROUP'" $DG
    then
        echo "runtime checks: daemongroup not set to $GROUP"
        return 13
    elif ! grep -q "^User\s$USER" $TP
    then
        echo "runtime checks: User not set to $USER"
        return 23
    elif ! grep -q "^Group\s$GROUP" $TP
    then
        echo "runtime checks: Group not set to $GROUP"
        return 24
    else
        echo "runtime checks: no errors found"
        return 0
    fi
}

fix()
{
    chown -R $USER:$GROUP $DG_LOGDIR
    chown -R $USER:$GROUP $TP_LOGDIR
    chown -R $USER:$GROUP /var/run/tinyproxy
    if grep -q "^daemonuser = .*" $DG
    then
        sed -i "s/^daemonuser = .*/daemonuser = '$USER'/" $DG
    else
        echo "daemonuser = '$USER'" >> $DG
    fi
    if grep -q "^daemongroup = .*" $DG
    then
        sed -i "s/^daemongroup = .*/daemongroup = '$GROUP'/" $DG
    else
        echo "daemongroup = '$GROUP'" >> $DG
    fi
    if grep -q "^User\s" $TP
    then
        sed -i "s/^User\s.*/User $USER/" $TP
    else
        echo "User $USER" >> $TP
    fi
    if grep -q "^Group\s" $TP
    then
        sed -i "s/^Group\s.*/Group $GROUP/" $TP
    else
        echo "Group $GROUP" >> $TP
    fi
    return $?
}


if [ $# -ne 1 ]
then
    echo "usage : $0 <fix/status/status_for_GUI>"
    exit 1
fi


if [ $1 = 'fix' ]
then
    fix
    if status
    then
    echo "fix successful"
    else
    echo "fix unsuccessful"
    fi
fi

if [ $1 = 'status' ]
then
    status
fi

if [ $1 = 'status_for_GUI' ]
then
    status >/dev/null
    echo -n $?
fi
