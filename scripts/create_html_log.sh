#!/bin/bash
source yourpath/file_paths.sh
#TMP=`mktemp`
LOGDIR=/tmp/dglog
mkdir -p $LOGDIR
ACCESSLOG=$LOGDIR/access.log
HTMLLOG=$LOGDIR/access.html
cd $DG_LOGDIR
tac access.log access.log.1 access.log.2 access.log.3 access.log.4 access.log.5 >$ACCESSLOG
yourpath/log2html.pl $ACCESSLOG $HTMLLOG
