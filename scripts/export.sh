#!/bin/bash
source yourpath/file_paths.sh

if [ $# -ne 1 ]
then
	echo "usage : $0 <filename>"
	exit 0
fi

DIR=$(dirname "$1")
filename=$DIR/$(basename "$1" .dgs).dgs

echo "Running from $(pwd)"
echo "===>exporting $filename"
tar -Pczvf "$filename" $DG $DG_LISTS
echo "Running from $(pwd)"
echo "===>exported $filename"
