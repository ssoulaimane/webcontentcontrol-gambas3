#!/bin/bash

for prog in $@;
do
	G=$( ls -l $(which $prog) | awk '{print $4}' )
	if [ $G = 'root' ]
	then
		echo "Adding $prog to programmer group"
		yourpath/set_program_to_group.sh $prog programmer
	else
		echo "Sorry, you are not allowed to modify this programs permissions."
	fi
done
