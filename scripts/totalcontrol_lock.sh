#!/bin/bash

source yourpath/file_paths.sh
source yourpath/import_export_functions.sh
source yourpath/common_functions.sh

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ] && [ $# -ne 7 ]
then
        echo "usage :"
        echo "`basename $0` USERNAME [ZEFILE M H DOM MON DOW]"
	echo "min(0 - 59)"
	echo "hour(0 - 23)"
	echo "day of month(1 - 31)"
	echo "month(1 - 12)"
	echo "day of week(0 - 6)(Sunday=0)"
        exit 0
fi

USERNAME=$1
ZEFILE=$2
M=$3
H=$4
DOM=$5
MON=$6
DOW=$7

#create sudoers.locked
sed "/# User alias specification/a\User_Alias     PARTIALADMINS = $USERNAME" yourpath/sudoers.locked.template >yourpath/sudoers.locked

#check sudoers.locked
if ! ( visudo -c -f yourpath/sudoers.locked )
then
    echo "ERROR: yourpath/sudoers.locked was not parsed successfully."
    exit 1
fi

#locked groups
echo "-->locking groups"
yourpath/remove_user_from_group.sh $USERNAME gamer
yourpath/remove_user_from_group.sh $USERNAME programmer
yourpath/remove_user_from_group.sh $USERNAME browsers

#backup files
echo "-->making backups"
cp -v /etc/sudoers yourpath/sudoers.$(date +%Y%m%d_%H%M%S)
cp -v /boot/grub/menu.lst yourpath/menu.lst.$(date +%Y%m%d_%H%M%S)

#replace files
echo "-->Locking system"
#cp -v yourpath/menu.lst.locked /boot/grub/menu.lst
sed -i s/^#password/password/ /boot/grub/menu.lst
cp -v yourpath/sudoers.locked /etc/sudoers

if [ ! -z $ZEFILE ]
then
    if [ ! -s $ZEFILE ]
    then
	echo "ERROR: $ZEFILE does not exist or is empty."
	exit 1
    fi
    echo "adding $ZEFILE unlocking to crontab"
    sed "\$a $M $H $DOM $MON $DOW chmod 755 $ZEFILE" yourpath/crontab.locked.template >yourpath/crontab.locked
    echo "chmod 000 $ZEFILE"
    chmod 000 $ZEFILE
else
    cp -v yourpath/crontab.locked.template yourpath/crontab.locked
fi
echo "================"
cat yourpath/crontab.locked.template
echo "================"
cat yourpath/crontab.locked
echo "================"

echo "-->activating time lock"
crontab yourpath/crontab.locked

#lock proxy settings
echo "-->Locking firefox proxy settings"
yourpath/firefox_setup.sh lock

#lock WPA
echo "-->Locking WPA interfaces"
yourpath/ifupdown_setup.sh lock

#Make sure all services run at startup
echo "-->Activating services"
yourpath/service_setup.sh firehol on
yourpath/service_setup.sh tinyproxy on
yourpath/service_setup.sh dansguardian on

#restart services
echo "-->Restarting all services"
restart_all_nogui

echo "Would you like to change the root password?"
read ans
case $ans in
        y|Y|yes) passwd root;;
esac

echo "System lockdown successful."
