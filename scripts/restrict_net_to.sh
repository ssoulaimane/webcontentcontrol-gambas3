#!/bin/bash
set -u

SCRIPT_PATH=yourpath
source $SCRIPT_PATH/import_export_functions.sh

# Check if all parameters are present
# If no, exit
if [ $# -ne 3 ]
then
        echo "usage :"
        echo "`basename $0` WHITELIST USER TIME(HH:MM) "
        exit 0
fi

WHITELIST=$1
USERNAME=$2
TIME=$3

#check time
if ! ( $SCRIPT_PATH/nonroot/check_time.py "$TIME" )
then
    exit 1
fi

#check whitelist
if ! ( sudo -u $USERNAME $SCRIPT_PATH/nonroot/check_sitelist.py $WHITELIST )
then
    exit 1
fi

#exit 0

#save current settings
CURRENT_SETTING=`cat $TC_STATUS`
save_current_settings

#change status
echo "$SCRIPT_PATH/whitelist_surfing.tcs" > $TC_STATUS

#Activate blanket block
# sed 's/\*\*$/#**/' on >off
# sed 's/\#\*\*$/**/' off >on
sed -i '$a**' /etc/dansguardian/lists/bannedsitelist

#replace exceptionlist
cp -v $WHITELIST /etc/dansguardian/lists/exceptionsitelist

#restart dg
yourpath/servicectl.sh restart dansguardian

#make sure previous settings get reloaded at specified time
COMMAND="$SCRIPT_PATH/totalcontrol_load.sh $USERNAME $CURRENT_SETTING"
echo "COMMAND=$COMMAND"
echo "$COMMAND" | at $TIME
# JOB="$M $H $DOM $MON $DOW $COMMAND"
# echo "JOB=$JOB"
# $SCRIPT_PATH/nonroot/crontab_add.sh "$JOB"
