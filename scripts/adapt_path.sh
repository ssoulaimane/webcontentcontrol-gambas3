#!/bin/bash

set -u

THIS_SCRIPT=`basename $0`
TMP=`mktemp` || ( echo "ERROR: Could not create temporary file." && exit 1 )

#adapts variables for use with sed
adapt_var()
{
    echo $1 >$TMP
    sed -i 's/\/$//g' $TMP #replace trailing "/"
    sed -i 's/\//\\\//g' $TMP #replace "/" with "\/"
}

# adapt_var $1
# var=`cat $TMP`
# echo $var
# exit 0

if [ $# -lt 3 ]
then
        echo "usage : $0 <REPLACEMENT> <PLACEHOLDER> FILE1 [FILE2...]"
	echo "Replaces placeholder with $PLACEHOLDER in all files except this script."
        exit 0
fi

#check that all temporary files are read-write
if ! ( [ -r $TMP ] && [ -w $TMP ] )
then
    echo "ERROR: $TMP does not have read/write permissions"
    exit -1
fi

PLACEHOLDER=$2

#adapt variables for sed
adapt_var $1
REPLACEMENT=`cat $TMP`
adapt_var $PLACEHOLDER
PLACEHOLDER2=`cat $TMP`

for var in "${@:3}"
do
    if [ -f "$var" ]
    then
#       echo "Processing $var"
      PLACEHOLDER=$2
      DIR=$3
      sed -i s/$PLACEHOLDER2/$REPLACEMENT/g "$var"
    else
      echo "Skipping $var. Not a file."
    fi
done
