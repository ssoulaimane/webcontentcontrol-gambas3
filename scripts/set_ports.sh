#!/bin/bash
# set -x

source yourpath/file_paths.sh

if [ $# -ne 2 ]
then
	echo "usage : $0 <proxyport> <filterport>"
	exit 0 
fi

filterport=`awk '/^filterport/ {print $3}' $DG`
proxyport=`awk '/^proxyport/ {print $3}' $DG`
echo "proxyport=$proxyport"
echo "filterport=$filterport"

gksudo -- sed -i.bak "s/^proxyport = [0-9]*/proxyport = $1/" $DG >/dev/null
gksudo -- sed -i.bak "s/^iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport [0-9]* -m owner ! --uid-owner webcontentcontrol -j DROP/iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport $1 -m owner ! --uid-owner webcontentcontrol -j DROP/" $FH >/dev/null
gksudo -- sed -i.bak "s/^Port [0-9]\+/Port $1/" $TP >/dev/null

gksudo -- sed -i.bak "s/^filterport = [0-9]*/filterport = $2/" $DG >/dev/null
gksudo -- sed -i.bak "s/^transparent_squid [0-9]*/transparent_squid $2/" $FH >/dev/null

echo "========================"
filterport=`awk '/^filterport/ {print $3}' $DG`
proxyport=`awk '/^proxyport/ {print $3}' $DG`
echo "proxyport=$proxyport"
echo "filterport=$filterport"
