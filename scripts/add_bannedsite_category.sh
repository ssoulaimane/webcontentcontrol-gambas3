#!/bin/bash

FILE=/etc/dansguardian/lists/bannedsitelist

for category in $@;
do
	#make sure it's there to avoid dansguardian not starting
	if test -s /etc/dansguardian/lists/blacklists/$category/domains
	then
	    if grep "^.Include</etc/dansguardian/lists/blacklists/$category/domains>"
	    then
	      echo "WARNING: category $category already banned"
	    else
	      echo "Adding $category"
	      sed -i '$a .Include</etc/dansguardian/lists/blacklists/'$category'/domains>'  $FILE >/dev/null
	    fi
	else
	    echo "ERROR: Category not found"
	fi
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
