#!/bin/bash
source yourpath/file_paths.sh

get_status()
{
    if grep dansguardian_log.pl $DG_INI
    then
	status='ON'
	echo "HTML log creation $status"
	return 0
    else
	status='OFF'
	echo "HTML log creation $status"
	return 1
    fi
}

if [ $# -ne 2 ]
then
        echo "usage : $0 <on/off/status> <location of dansguardian_log.pl>"
        exit 0
fi

SCRIPT_DIR=$2

if [ $1 = 'on' ]
then
    echo "ON"
    if ! get_status
    then
    #sed is amazing! :)
    sed -i.bak '$i'$SCRIPT_DIR'/dansguardian_log.pl' $DG_INI
    fi
    get_status
    exit 0
fi

if [ $1 = 'off' ]
then
    echo "OFF"
    get_status
    sed -i.bak '/dansguardian_log.pl/d' $DG_INI
    get_status
    exit 0
fi

if [ $1 = 'status' ]
then
    echo "STATUS"
    get_status
    exit 0
fi

echo "invalid argument"
echo "usage : $0 <on/off/status>"
