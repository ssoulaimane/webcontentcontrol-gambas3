#!/bin/bash
# set -x #for debugging
set -u

status()
{
    if yourpath/servicectl.sh is-enabled "$SERVICE" >/dev/null 2>&1
    then
	echo "STATUS: $SERVICE is activated. It will run on startup."
	return 1
    else
    echo "STATUS: $SERVICE is deactivated. It will not run on startup."
    return 0
    fi
}

activate_service()
{
    if [ $SERVICE = 'firehol' ]; then yourpath/servicectl.sh enable firehol; fi
    if [ $SERVICE = 'tinyproxy' ]; then yourpath/servicectl.sh enable tinyproxy; fi
    if [ $SERVICE = 'dansguardian' ]; then yourpath/servicectl.sh enable dansguardian; fi
}

deactivate_service()
{
    yourpath/servicectl.sh disable "$SERVICE"
}

if [ $# -ne 2 ]
then
        echo "usage : $0 <service> <on/off/status/status_for_GUI>"
        exit 0
fi

SERVICE=$1
ACTION=$2

if [ $ACTION = 'on' ]
then
    status
    activate_service
    status
    if [ $? -eq 1 ]
    then
	echo "Activation successful"
    else
	echo "Activation not successful"
    fi
fi

if [ $ACTION = 'off' ]
then
    status
    deactivate_service
    status
    if [ $? -eq 0 ]
    then
	echo "Deactivation successful"
    else
	echo "Deactivation not successful"
    fi
fi

if [ $ACTION = 'status' ]
then
    status
    exit $?
fi

if [ $ACTION = 'status_for_GUI' ]
then
    status >/dev/null
    echo -n $?
fi
