#!/bin/bash
#modify the following paths to "./" for example if you want to work locally.
#All scripts using "source yourpath/file_paths.sh" will use those variables.
DG=/etc/dansguardian/dansguardian.conf
FH=/etc/firehol/firehol.conf
if [ -e /etc/tinyproxy.conf ]
then
    TP=/etc/tinyproxy.conf
else
    TP=/etc/tinyproxy/tinyproxy.conf
fi

DG_INI=/etc/init.d/dansguardian
FH_INI=/etc/default/firehol

SQ=/etc/squid/squid.conf
SQ3=/etc/squid3/squid.conf
SQG=/etc/squid/squidGuard.conf
SQG_DOMS=/var/lib/squidguard/db/weapons/domains

DG_LISTS=/etc/dansguardian/lists
TP_FILTER=/etc/tinyproxy/filter

FF_CFG=/usr/share/doc/firefox-3.0/firefox.cfg
FF_JS=/etc/firefox/pref/firefox.js

# /etc/firefox-3.0/pref/firefox.js
# /etc/firefox/pref/firefox.js
# /usr/lib/firefox-3.6.3/defaults/pref/firefox.js

FF_JS1=/etc/firefox/pref/firefox.js
FF_JS2=/etc/firefox-3.0/pref/firefox.js
FF_JS3=/opt/swiftfox/defaults/pref/firefox.js
FF_JS4=/usr/lib/firefox-3.0.3/defaults/preferences/firefox.js

FF_CFG1=/usr/lib/firefox/firefox.cfg
FF_CFG2=/usr/share/doc/firefox-3.0/firefox.cfg

DG_LOGDIR=/var/log/dansguardian
DG_DIR=/etc/dansguardian

TC_BANNED=yourpath/tc_banned
TC_ALLOWED=yourpath/tc_allowed
TC_DESCRIPTION=yourpath/tc_description
TC_STATUS=yourpath/tc_status

IFUPDOWN=/etc/wpa_supplicant/ifupdown.sh
TP_LOGDIR=/var/log/tinyproxy
