#!/bin/bash
#How to kill and logout user $1
set -e -u

if [ $# -ne 0 ] && [ $# -ne 1 ]
then
    echo "usage : $0 [username]"
    exit -1
fi

if [ $# -eq 0 ]
then
    USERNAME=$USER
fi

if [ $# -eq 1 ]
then
    USERNAME=$1
fi

echo "Force logging out $USERNAME"
if [ $USERNAME = 'root' ]
then
	echo "ERROR: You are trying to log out root. This may hang the system."
	echo "I'm cowardly refusing to do this. Exiting."
	exit -1
fi
skill -KILL -u $USERNAME

exit 0
