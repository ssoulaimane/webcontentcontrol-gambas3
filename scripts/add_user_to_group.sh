#!/bin/bash
set -e -u

if [ $# -ne 2 ]
then
	echo "usage : $0 <user> <group>"
	exit -1 
fi
group=$2
username=$1

echo "Adding $username to $group"
sudo usermod -a -G $group $username
