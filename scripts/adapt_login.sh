#!/bin/bash

THIS_SCRIPT=`basename $0`
PLACEHOLDER="yourlogin"
DIR=.

if [ $# -lt 1 ] || [ $# -gt 3 ]
then
        echo "usage : $0 <$PLACEHOLDER> [placeholder=$PLACEHOLDER] [DIR=.]"
	echo "Replaces placeholder with $PLACEHOLDER in all files except this script."
        exit 0
fi

if [ $# -eq 2 ]
then
	PLACEHOLDER=$2
fi

if [ $# -eq 3 ]
then
	PLACEHOLDER=$2
	DIR=$3
fi

cd $DIR
find . -type f | grep -v $THIS_SCRIPT | xargs -n1 sed -i s/$PLACEHOLDER/$1/g
