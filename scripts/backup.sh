#! /bin/bash
DATE=$(date +%Y%m%d_%H%M%S)

#ARCHIVE=yourpath/dansguardian_backup_$DATE.tar.gz

BASENAME="yourpath/dansguardian_backup_"
ARCHIVE=$BASENAME$DATE.tar.gz

purge_backups()
{
	echo "Warning: Older backups will now be removed."
	#ls $BASENAME*.tar.gz
	rm -iv $BASENAME*.tar.gz
}

echo "Would you like to remove older backups first?(y/n/q)"
read ans
case $ans in
	y|Y|yes) purge_backups;;
	q) echo "quitting" && exit 0;;
	*) echo "Ok, not removing older backups.";;
esac

echo "Backing up dansguardian configuration"
tar -Pczvf $ARCHIVE /etc/dansguardian/lists/ --exclude=blacklists
exit 0
