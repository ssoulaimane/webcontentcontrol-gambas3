#!/bin/bash

FILE=/etc/dansguardian/lists/weightedphraselist

for category in $@;
do
	#make sure it's there to avoid dansguardian not starting
	if test -s /etc/dansguardian/lists/phraselists/$category/weighted
	then
	    echo "Adding $category"
	    sed -i '$a .Include</etc/dansguardian/lists/phraselists/'$category'/weighted>'  $FILE >/dev/null
	else
	    echo "ERROR: Category not found"
	fi
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
