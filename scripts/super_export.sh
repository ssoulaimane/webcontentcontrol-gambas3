#!/bin/bash
#set -x

source yourpath/import_export_functions.sh

if [ "$1" = '-h' ] || [ $# -eq 0 ]
then
        echo "usage :"
        echo "`basename $0` ARCHIVE [BLACKLISTS(1/0)] [TP_FH_FF(1/0)] [TC_FILES(1/0)]"
        exit 0
fi

echo "args=$@"
export_settings "$@"
