#!/bin/bash
# set -e -u
#set -x
source yourpath/file_paths.sh

# source ../parentalcontrol/file_paths.sh

add_extension()
{
	EXT=$2
	DIR=$(dirname "$1")
	filename=$DIR/$(basename "$1" .$EXT).$EXT
	echo $filename
}

remove_extension()
{
	EXT=$2
	DIR=$(dirname "$1")
	filename=$DIR/$(basename "$1" .$EXT)
	echo $filename
}

import_settings()
{
    ARCHIVE=$( add_extension "$1" tcs )
    #extract archive
    echo "Running from $(pwd)"
    echo "===>importing $ARCHIVE"
    tar -Pxzvf "$ARCHIVE"
    echo "Running from $(pwd)"
    echo "===>imported $ARCHIVE"
    echo "$ARCHIVE" > $TC_STATUS
    echo "==========="
    echo "$TC_STATUS:"
    cat $TC_STATUS
    echo "==========="
}

export_settings()
{
    #default values
    BLACKLISTS='0'
    TP_FH_FF='0'
    TC_FILES='0'

    #user defined values
    ARCHIVE=$( add_extension "$1" tcs )
    if [ -n "$2" ];then BLACKLISTS=$2;fi;
    if [ -n "$3" ];then TP_FH_FF=$3;fi;
    if [ -n "$4" ];then TC_FILES=$4;fi;

    echo "======================"
    echo "ARCHIVE=$ARCHIVE"
    echo "BLACKLISTS=$BLACKLISTS"
    echo "TP_FH_FF=$TP_FH_FF"
    echo "TC_FILES=$TC_FILES"
    echo "======================"

    #create filelist
    FILELIST=" $DG_DIR"
    if [ $TP_FH_FF = '1' ];then FILELIST+=" $TP $FH $FH_INI $FF_JS" ;fi;
    if [ $TC_FILES = '1' ];then FILELIST+=" $TC_DESCRIPTION $TC_BANNED $TC_ALLOWED" ;fi;
    if [ $BLACKLISTS = '0' ];then FILELIST+=" --exclude=blacklists" ;fi;
    #create archive
    echo "Running from $(pwd)"
    echo "===>exporting as $ARCHIVE"
    tar -Pczvf "$ARCHIVE" $FILELIST
    echo "Running from $(pwd)"
    echo "===>exported as $ARCHIVE"
}

save_current_settings()
{
    CURRENT_SETTING=`cat $TC_STATUS`
    export_settings $CURRENT_SETTING 0 0 1
}
