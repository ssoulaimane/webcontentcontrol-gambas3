#!/bin/bash

FILE=/etc/dansguardian/lists/bannedurllist

for category in $@;
do
	#make sure it's there to avoid dansguardian not starting
	if test -s /etc/dansguardian/lists/blacklists/$category/urls
	then
	    echo "Adding $category"
	    sed -i '$a .Include</etc/dansguardian/lists/blacklists/'$category'/urls>'  $FILE >/dev/null
	else
	    echo "ERROR: Category not found"
	fi
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
