#! /usr/bin/env python2

import string
import sys
import os
import re

def usage():
	print "A script to check if a list of domains contains any banned domains."
	print "Usage:"
	print sys.argv[0]+' FILE'

if len(sys.argv)==1:
	usage()
	sys.exit()

filename=sys.argv[1]

in_file=open(filename,'r')
for line in in_file:
	#if line == '\n'
	#
	if not line.strip():
		print 'empty line'
	elif re.match('#',line):
		print 'comment line'
	else:
	    line=line.rstrip('\n')
	    print '===>Checking '+line+' ...'
	    cmd='yourpath/nonroot/wget_site.sh '+line
	    print "Running: "+cmd
	    ret=os.system(cmd)
	    print 'ret=',ret
	    if( ret != 0 ):
		    in_file.close()
		    print '===>LIST CONTAINS BANNED SITES! :('
		    sys.exit(1)
in_file.close()
print 'LIST ACCEPTABLE :)'
sys.exit(0)
