#! /bin/bash
# to test if a site gets through. ;)
set -u

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` URL"
        exit 0
fi


TMP=$(mktemp)
echo $TMP
# TMPDIR=$(mktemp -d)
# echo $TMPDIR

# cd $TMPDIR
# wget -E -H -k -K -p $1 1>/dev/null 2>&1
if ! ( wget $1 -O $TMP )
then
    echo "==>Site could not be downloaded"
    exit 2
fi

# if ! (wget -E -H -k -K -p $1 2>$TMP )
# then
# 	echo "Site banned"
# 	exit 1
# fi

hint="Access has been Denied"
grep "$hint" $TMP
result=$?

# rm $TMP

# result=0 => hint found
# result=1 => hint not found
# result=2 => index.html not found
if [ $result -eq 0 ]
then
	echo "==>Site banned"
	exit 1
else
    if [ $result -eq 1 ]
    then
	    echo "==>Site not banned"
	    exit 0
    else
	    echo "==>Site could not be downloaded"
	    exit 2
    fi
fi

# echo "==========="
# cat $TMP
# echo "==========="
# 
# if [ -s $TMP ]
# then
# 	echo "Site banned"
# 	exit 1
# else
# 	echo "Site not banned"
# 	exit 0
# fi
