#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import tempfile
import sys
import re
import string
import datetime

def usage():
	print "A script to check if a given time is between now and closest settings change time."
	print "Usage:"
	print sys.argv[0]+' time'

def HMS_to_sec(name,H,M,S):
    t = S + M * 60 + H * 60 * 60
    print name+'(H:M:S)='+str(H)+":"+str(M)+":"+str(S)
    print name+'(sec)=',t
    return(t)

#timewindow function that works with normal timewindows (Tmin<=Tmax)
def normaltimewindow(t1,t2,t3):
    if t1<t2<t3:
	return True
    else:
	return False

def is_in_timewindow(t1,t2,t3):
    if t1<=t3:
	    print "normal timewindow"
	    if normaltimewindow(t1,t2,t3):
		    print "we are inside the normal timewindow"
		    return True
	    else:
		    print "we are outside the normal timewindow"
		    return False
    else:
	    print "inverse timewindow"
	    #Note: 86400 sec = 24:00:00
	    if normaltimewindow(t1,t2,86400) or normaltimewindow(0,t2,t3):
		    print "we are inside the inverse timewindow"
		    return True
	    else:
		    print "we are outside the inverse timewindow"
		    return False

def getmin(str):
	A=str.split('-')
	print "A=",A
	if A[0]=='*':
		return 0
	else:
		return int(A[0])

if len(sys.argv)!=2:
	usage()
	sys.exit(1)

#current time
now=datetime.datetime.now()
H=int(now.strftime("%H"))
M=int(now.strftime("%M"))
S=int(now.strftime("%S"))
t_now = HMS_to_sec("NOW",H,M,S)

#desired time
desired_time=sys.argv[1]
A=string.split(desired_time,':')
H=int(A[0])
M=int(A[1])
S=0
t_desired = HMS_to_sec("desired_time",H,M,S)

#compare to cronfile
filename = tempfile.mktemp()
os.system('crontab -l >'+filename)
infile = open(filename, "r")
if infile:
	print "tmpfile="+infile.name
	
	for line in infile:
          if not line.strip(): # skip empty or space padded lines
            continue
          if re.compile('^#').search(line) is not None: # skip commented lines
            continue
          else: # process line
		if re.findall(r'totalcontrol_load',line):
			print line
			A=string.split(line,' ')
			H=getmin(A[1])
			M=getmin(A[0])
			S=0
			t_cron=HMS_to_sec("t_cron",H,M,S)
			if is_in_timewindow(t_now,t_desired,t_cron):
			    print 'OK'
			else:
			    print 'not OK'
			    print 'Time not acceptable. :('
			    infile.close()
			    sys.exit(1)

	print 'Time accepted :)'
	infile.close()
	sys.exit(0)

print "Could not open file."
sys.exit(1)
