#!/bin/bash
# Check if all parameters are present
# If no, exit
set -e -u

SCRIPTS_PATH=yourpath

mkdir -p $HOME/.kde/Autostart
mkdir -p $HOME/.config/autostart
mkdir -p $HOME/.config/autostart

usage()
{
        echo "usage :"
        echo "`basename $0` on HH:MM DD.MM.YY"
        echo "`basename $0` on HH:MM"
        echo "`basename $0` force-on HH:MM DD.MM.YY"
        echo "`basename $0` force-on HH:MM"
        echo "`basename $0` off"
        exit 0
}

activate()
{
	CMD="$SCRIPTS_PATH/nonroot/forcelogout.sh off"
	echo "$CMD" | at $TIME $DATE
	echo "$SCRIPTS_PATH/logout.sh" >> $HOME/.bashrc
	ln -sf $SCRIPTS_PATH/logout.sh $HOME/.kde/Autostart/logout.sh
	ln -sf $SCRIPTS_PATH/logout.sh $HOME/.config/autostart/logout.sh
	cp -v $SCRIPTS_PATH/logout.sh.desktop $HOME/.config/autostart/

	echo "Stay out!" >$HOME/status
}

deactivate()
{
	sed -i.bak '/logout.sh/d' $HOME/.bashrc 
	rm -fv $HOME/.kde/Autostart/logout.sh
	rm -fv $HOME/.config/autostart/logout.sh
	rm -fv $HOME/.config/autostart/logout.sh.desktop

	echo "Welcome back!" >$HOME/status
}

if [ $# -eq 2 ] && [ $1 = 'on' ]
then
	echo "ON"
	TIME=$2
	DATE=""
	echo "Login will be disabled until $TIME $DATE. Proceed?"
	read ans
	case $ans in
	y|Y|yes) activate;;
	*) exit 0;;
	esac
	exit 0
fi

if [ $# -eq 3 ] && [ $1 = 'on' ]
then
	echo "ON"
	TIME=$2
	DATE=$3
	echo "Login will be disabled until $TIME $DATE. Proceed?"
	read ans
	case $ans in
	y|Y|yes) activate;;
	*) exit 0;;
	esac
	exit 0
fi

if [ $# -eq 2 ] && [ $1 = 'force-on' ]
then
	echo "ON"
	TIME=$2
	DATE=""
	echo "Login will be disabled until $TIME $DATE."
	activate
	exit 0
fi

if [ $# -eq 3 ] && [ $1 = 'force-on' ]
then
	echo "ON"
	TIME=$2
	DATE=$3
	echo "Login will be disabled until $TIME $DATE."
	activate
	exit 0
fi

if [ $# -eq 1 ] && [ $1 = 'off' ]
then
	echo "OFF"
	deactivate
	exit 0
fi

usage
