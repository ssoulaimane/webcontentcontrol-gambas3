#!/bin/bash
set -u

# Check if all parameters are present
# If no, exit
if [ $# -ne 2 ]
then
        echo "usage :" >&2
        echo "`basename $0` ARCHIVE FILE" >&2
        exit 0
fi

ARCHIVE="$1"
FILE="$2"

TMP=$(mktemp -d)
if tar -C $TMP -xzf "$ARCHIVE" "$FILE" 1>/dev/null 2>&1
then
    cat "$TMP/$FILE"
#     rm -rf $TMP
else
    echo "ERROR: $FILE not found in $ARCHIVE" >&2
    echo "Perhaps you meant one of these:" >&2
    tar -tzvf "$ARCHIVE" | grep "$FILE"
fi
