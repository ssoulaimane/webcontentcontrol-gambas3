#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import re
import sys
import os
import shutil
import getopt
import glob
import pygtk
pygtk.require('2.0')
import gtk
import os
import tempfile

category_dir='/etc/dansguardian/lists/blacklists/'
bannedsitelist='/etc/dansguardian/lists/bannedsitelist'
bannedurllist='/etc/dansguardian/lists/bannedurllist'
script_dir='/usr/share/webcontentcontrol/scripts/'

add_bannedurl_category_script = script_dir + 'add_bannedurl_category.sh'
add_bannedsite_category_script = script_dir + 'add_bannedsite_category.sh'

def add_bannedsite_category(categories):
  script_name = add_bannedsite_category_script;
  terminal="xterm -e "
  script="sudo "+script_name+" "
  command = terminal + script
  for category in categories:
    command += ' '+category
  print "=========================="
  print "===RUNNING COMMAND========"
  print "command:"
  print command
  print "=========================="
  os.system(command)
  print "=========================="
  print "===RUNNING COMMAND DONE==="
  print "=========================="

def add_bannedurl_category(categories):
  script_name = add_bannedurl_category_script;
  terminal="xterm -e "
  script="sudo "+script_name+" "
  command = terminal + script
  for category in categories:
    command += ' '+category
  print "=========================="
  print "===RUNNING COMMAND========"
  print "command:"
  print command
  print "=========================="
  os.system(command)
  print "=========================="
  print "===RUNNING COMMAND DONE==="
  print "=========================="
    
def check_domain(category):
  ret = False
  expr = re.compile('^ *\.Include *</etc/dansguardian/lists/blacklists/'+category+'/domains>')
    
  for line in open(bannedsitelist):
    match = expr.search(line)
    if match != None:
      print match.string
      ret = True
      
  return ret

def check_url(category):
  ret = False
  expr = re.compile('^ *\.Include *</etc/dansguardian/lists/blacklists/'+category+'/urls>')
  
  for line in open(bannedurllist):
    match = expr.search(line)
    if match != None:
      #print match.string
      ret = True
      
  return ret
      
class TextViewExample:
    domain_checkbox_list = [];
    url_checkbox_list = [];
    N_categories = 0;
    category_list = [];
    
    def close_application(self, widget):
        gtk.main_quit()

    def add_bannedsites(self, widget):
        print "===>Adding categories..."
        domain_list = []
        url_list = []
        for idx in range(self.N_categories):
          category = self.category_list[idx]
          if self.domain_checkbox_list[idx].get_active():
            print 'Adding '+category+' domains'
            domain_list.append(category)
          if self.url_checkbox_list[idx].get_active():
            print 'Adding '+category+' urls'
            url_list.append(category)
            
        add_bannedsite_category(domain_list)
        add_bannedurl_category(url_list)
        
    def __init__(self):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_resizable(True)
        window.set_default_size(500, 500)

        window.connect("destroy", self.close_application)
        window.set_title("Add banned categories")
        window.set_border_width(0)

        vbox = gtk.VBox(False, 0)
        window.add(vbox)
        vbox.show()

        # create a new scrolled window.
        scrolled_window = gtk.ScrolledWindow()
        scrolled_window.set_border_width(10)

        # the policy is one of POLICY AUTOMATIC, or POLICY_ALWAYS.
        # POLICY_AUTOMATIC will automatically decide whether you need
        # scrollbars, whereas POLICY_ALWAYS will always leave the scrollbars
        # there. The first one is the horizontal scrollbar, the second, the
        # vertical.
        scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)

        # The dialog window is created with a vbox packed into it.
        vbox.pack_start(scrolled_window, True, True, 0)
        scrolled_window.show()
    
        self.category_list = []
        for category in os.listdir(category_dir):
          if category != 'CATEGORIES':
            self.category_list.append(category)
        self.category_list.sort()
        self.N_categories = len(self.category_list)
        print "N_categories = ",self.N_categories
        
        # create a table of 10 by 10 squares.
        table = gtk.Table(2, self.N_categories, False)

        # set the spacing to 10 on x and 10 on y
        table.set_row_spacings(10)
        table.set_col_spacings(10)

        # pack the table into the scrolled window
        scrolled_window.add_with_viewport(table)
        table.show()

        # this simply creates a grid of toggle buttons on the table
        # to demonstrate the scrolled window.
        for idx in range(self.N_categories):
          category = self.category_list[idx]
          
          #print category
          #print os.listdir(category_dir+'/'+category)
          check = gtk.CheckButton(category+' domains')
          check.set_active(check_domain(category))
          check.show()
          table.attach(check, 0, 1, idx, idx+1)
          self.domain_checkbox_list.append(check)
          if not(os.path.exists(category_dir+'/'+category+'/domains')):
            check.set_sensitive(False)
            
          check = gtk.CheckButton(category+' urls')
          check.set_active(check_url(category))
          check.show()
          table.attach(check, 1, 2, idx, idx+1)
          self.url_checkbox_list.append(check)
          if not(os.path.exists(category_dir+'/'+category+'/urls')):
            check.set_sensitive(False)
              
        separator = gtk.HSeparator()
        vbox.pack_start(separator, False, True, 0)
        separator.show()

        button = gtk.Button("Add")
        button.set_size_request(-1,50)
        button.connect("clicked", self.add_bannedsites)
        vbox.pack_start(button, False, True, 0)
        button.set_flags(gtk.CAN_DEFAULT)
        button.grab_default()
        button.show()
        window.show()

def main():
    gtk.main()
    return 0

if __name__ == "__main__":
    TextViewExample()
    main()
