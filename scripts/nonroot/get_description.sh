#!/bin/bash
set -u

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :" >&2
        echo "`basename $0` ARCHIVE" >&2
        exit 0
fi

ARCHIVE="$1"
FILE=tc_description

fullname=$( ( tar -tzf "$ARCHIVE" | grep "$FILE" ) 2>/dev/null )

TMP=$(mktemp -d)
if tar -C $TMP -xzf "$ARCHIVE" "$fullname" 1>/dev/null 2>&1
then
    cat "$TMP/$fullname"
else
    echo "ERROR: $fullname not found in $ARCHIVE" >&2
    echo "Perhaps you meant one of these:" >&2
    tar -tzvf "$ARCHIVE" | grep "$FILE"
fi
