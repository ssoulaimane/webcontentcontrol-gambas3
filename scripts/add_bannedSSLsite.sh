#!/bin/bash

# echo list1=$@
# echo list2=$*

for url in $@;
do
        echo "Adding $url"
	sed -i '$a '$url'' /var/lib/squidguard/db/weapons/domains >/dev/null
done

echo "updating squidguard database"
/usr/bin/squidGuard -C all
echo "restarting squid"
yourpath/servicectl.sh restart squid
