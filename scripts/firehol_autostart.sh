#!/bin/bash
source yourpath/file_paths.sh

get_status()
{
    if yourpath/servicectl.sh is-enabled firehol >/dev/null 2>&1
    then
        status=on
    else
        status=off
    fi
    echo "START_FIREHOL=$status"
}

if [ $# -ne 1 ]
then
        echo "usage : $0 <on/off/status>"
        exit 0
fi

if [ $1 = 'on' ]
then
    echo "ON"
    get_status
    yourpath/servicectl.sh enable firehol
    get_status
    exit 0
fi

if [ $1 = 'off' ]
then
    echo "OFF"
    get_status
    yourpath/servicectl.sh disable firehol
    get_status
    exit 0
fi

if [ $1 = 'status' ]
then
    echo "STATUS"
    get_status
    exit 0
fi

echo "invalid argument"
echo "usage : $0 <on/off/status>"
