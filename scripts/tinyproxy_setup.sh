#!/bin/bash
# set -x #for debugging
source yourpath/file_paths.sh

check_values()
{
    grep ^User $TP
    grep ^Group $TP
    grep ^Port $TP
    ls -ld $TP_LOGDIR
}

config_set()
{
    name=$1
    value=$2
    if grep -q "^$name " $TP && sed -i "s/^$name .*/$name $value/" $TP
    then
        return
    # ................................... replace first match only
    elif grep -q "^#$name " $TP && sed -i "0,/^#$name / s/^#$name .*/$name $value/" $TP
    then
        return
    else
        echo "$name $value" >> $TP
        return
    fi
}

if [ $# -ne 3 ]
then
	echo "============="
        echo "usage :"
        echo "set values : `basename $0` USER GROUP PORT"
        echo "check values : `basename $0`"
	echo "============="
	check_values
	exit 0
fi

USER=$1
GROUP=$2
PORT=$3

config_set User $USER
config_set Group $GROUP
config_set Port $PORT

if [ -e $TP_LOGDIR ]
then
	chown -R $USER:$GROUP $TP_LOGDIR
fi

check_values
