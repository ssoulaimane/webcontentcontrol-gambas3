#!/bin/bash
source yourpath/common_functions.sh

function check_ports {
    echo "checking ports"
    startcheck
}

function start {
    echo "starting all"
    gksudo yourpath/servicectl.sh start firehol;
    gksudo yourpath/servicectl.sh start tinyproxy;
    gksudo yourpath/servicectl.sh start dansguardian;
}

function stop {
    echo "stopping all"
    gksudo yourpath/servicectl.sh stop firehol;
    gksudo yourpath/servicectl.sh stop tinyproxy;
    gksudo yourpath/servicectl.sh stop dansguardian;
}

set_ports()
{
    echo "setting ports"
    yourpath/set_ports.sh $1 $2
}

proxyport_start_index=3128
proxyport_end_index=3129
filterport_start_index=8080
filterport_end_index=8081

for (( proxyport = $proxyport_start_index ; proxyport <= $proxyport_end_index ; proxyport++ ))
do
    for (( filterport = $filterport_start_index ; filterport <= $filterport_end_index ; filterport++ ))
    do
	echo proxyport=$proxyport filterport=$filterport
	echo "==========================================="
	set_ports $proxyport $filterport
	echo "==========================================="
	stop
	echo "==========================================="
	start
	echo "==========================================="
	if check_ports
	then
	    exit 0
	fi
	echo "==========================================="
    done;
done;
