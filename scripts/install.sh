#!/bin/bash
#Does all necessary stuff for protection to work.
# fh.conf->set ports+full protection
# dg.conf->set ports
# tp.conf->set ports
# default/fh->NO->YES
# init.d/dg->add html logs
# start all

source yourpath/file_paths.sh

if [ $# -ne 1 ]
then
        echo "usage : $0 <maindir>"
        exit 1
fi

MAINDIR=$1

if [ -e $MAINDIR/configured ]
then
    echo "System already configured. Skipping configuration."
    exit 0
fi

yourpath/export_all.sh "$MAINDIR/backup"
# yourpath/html_logs.sh on "$1/parentalcontrol" #replaced by log2html.pl+create_html_log.sh
echo "" >$FH  # clear firehol config file
sed -i.bak 's/START_FIREHOL=.*/START_FIREHOL=YES/' $FH_INI  # workaround a bug in some distros
yourpath/firehol_autostart.sh on
yourpath/firehol_setup.sh on
yes yes | yourpath/get-iana.sh #to create RESERVED_IPS file for firehol

# add custom group:user if not exists
getent group  webcontentcontrol > /dev/null || groupadd webcontentcontrol
getent passwd webcontentcontrol > /dev/null || useradd webcontentcontrol -g webcontentcontrol

chown -R webcontentcontrol:webcontentcontrol /var/run/tinyproxy $DG_LOGDIR $TP_LOGDIR

yourpath/tinyproxy_setup.sh webcontentcontrol webcontentcontrol 3128
rm /tmp/.dguardianipc /tmp/.dguardianurlipc # resolve some dansguardian ownership problems
yourpath/dansguardian_setup.sh on
# 	yourpath/firefox_setup.sh lock
yourpath/autosetports_nogui.sh
touch $MAINDIR/configured
