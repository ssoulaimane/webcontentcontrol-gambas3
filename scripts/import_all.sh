#!/bin/bash

if [ $# -ne 1 ]
then
	echo "usage : $0 <filename>"
	exit 0
fi

DIR=$(dirname "$1")
filename=$DIR/$(basename "$1" .dgs.full).dgs.full

echo "Running from $(pwd)"
echo "===>importing $filename"
tar -Pxzvf "$filename"
echo "Running from $(pwd)"
echo "===>imported $filename"
