#!/bin/bash

FILE=/etc/dansguardian/lists/bannedregexpheaderlist

for arg in "$@";
do
        echo "Adding $arg"
	sed -i '$a'"$arg"'' $FILE >/dev/null
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
