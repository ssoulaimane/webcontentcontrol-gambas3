#!/bin/bash

source yourpath/common_functions.sh

ProxyPort=$1
FilterPort=$2

yourpath/set_ports.sh $ProxyPort $FilterPort
stop_all_nogui
stopcheck
start_all_nogui
startcheck
