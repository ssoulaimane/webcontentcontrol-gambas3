#!/bin/bash
if ! [ -s $1 ]
then
    echo "ERROR: File does not exist or is empty"
    exit 1
fi

TMP1=`mktemp` || ( echo "ERROR: Could not create temporary file." && exit 1 )
TMP2=`mktemp` || ( echo "ERROR: Could not create temporary file." && exit 1 )

#check that all temporary files are read-write
if ! ( [ -r $TMP1 ] && [ -w $TMP1 ] )
then
    echo "ERROR: $TMP1 does not have read/write permissions"
    exit -1
fi
if ! ( [ -r $TMP2 ] && [ -w $TMP2 ] )
then
    echo "ERROR: $TMP2 does not have read/write permissions"
    exit -1
fi

CMD='sudo yourpath/add_bannedsite.sh'

cp $1 $TMP1
sed -i 's/^http:\/\///' $TMP1 >/dev/null
sed -i 's/^https:\/\///' $TMP1 >/dev/null
sed -i 's/^www\.//' $TMP1 >/dev/null
sed -i 's/\/.*$//' $TMP1 >/dev/null
sort -u $TMP1 -o $TMP1

echo "=====SITES====="
cat $TMP1
echo "==============="

sed -i '1i '"$CMD"'' $TMP1 >/dev/null
tr '\n' ' ' <$TMP1 >$TMP2
echo >>$TMP2

echo "====COMMAND===="
cat $TMP2
echo "==============="

echo "Execute command?"
read ans
case $ans in
  y|Y|yes) bash $TMP2 && exit 0;;
  *) exit 1;;
esac
