#!/bin/bash
# set -x #for debugging
set -u
# source yourpath/file_paths.sh
IFUPDOWN=/etc/wpa_supplicant/ifupdown.sh

lock()
{
    sed -i "/do_start () {/a\	yourpath/servicectl.sh restart firehol" $IFUPDOWN >/dev/null
}

unlock()
{
    sed -i "/yourpath\/servicectl.sh restart firehol/d" $IFUPDOWN >/dev/null
}

status()
{
    if grep -q "yourpath/servicectl.sh restart firehol" $IFUPDOWN
    then
	echo "STATUS: WPA is locked"
	return 1
    else
	echo "STATUS: WPA is unlocked"
	return 0
    fi
}

if [ $# -ne 1 ]
then
        echo "usage : $0 <lock/unlock/status/status_for_GUI>"
        exit 0
fi

if [ $1 = 'lock' ]
then
    status
    lock
    status
    if [ $? -eq 1 ]
    then
	echo "Locking successful"
    else
	echo "Locking not successful"
    fi
fi

if [ $1 = 'unlock' ]
then
    status
    unlock
    status
    if [ $? -eq 0 ]
    then
	echo "Unlocking successful"
    else
	echo "Unlocking not successful"
    fi
fi

if [ $1 = 'status' ]
then
    status
fi

if [ $1 = 'status_for_GUI' ]
then
    status >/dev/null
    echo -n $?
fi
