#!/bin/bash

FILE=/etc/dansguardian/lists/weightedphraselist

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` [FILE]"
	echo "This script will attach the contents of FILE to $FILE"
	echo "Note: It will only accept lines ending with <[0-9]*>, i.e. positive weights. ;)"
        exit 0
fi

add_it()
{
    cat $TMP >>$FILE
    echo "restarting dansguardian"
    yourpath/servicectl.sh restart dansguardian
}

TMP=`mktemp` || ( echo "ERROR: Could not create temporary file." && exit 1 )

#check that all temporary files are read-write
if ! ( [ -r $TMP ] && [ -w $TMP ] )
then
    echo "ERROR: $TMP does not have read/write permissions"
    exit -1
fi

#sed 's/< *-/</' $1 >$TMP
sed -n '/<[0-9]*>$/p' $1 >$TMP #Only allow lines ending with <positive_number> ;)

echo "=====WEIGHTED PHRASES====="
cat $TMP
echo "=========================="

echo "Add those weighted phrases?"
read ans
case $ans in
  y|Y|yes) add_it && exit 0;;
  *) exit 1;;
esac

exit 2
