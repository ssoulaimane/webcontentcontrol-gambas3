#! /bin/bash
SCRIPTNAME=`basename $1`
FILE=yourpath/$SCRIPTNAME
DIR=yourpath/disabled_scripts

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` [SCRIPTNAME]"
        exit 0
fi

if ! mkdir -p $DIR
then
    echo "ERROR: Could not create $DIR." && exit 4
fi

if test -s $FILE
then
    echo "Move $FILE to $DIR?"
    read ans
    case $ans in
    y|Y|yes) mv -iv $FILE $DIR && echo "$SCRIPTNAME disabled successfully" && exit 0;;
    *) echo "Exiting" && exit 1;;
    esac
else
    echo "ERROR: $FILE not found" && exit 2
fi
exit 3
