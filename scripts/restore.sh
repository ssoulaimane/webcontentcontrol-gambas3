#! /bin/bash

BASENAME="yourpath/dansguardian_backup_"

if [ $# -eq 1 ]
then
    DATE=$1
    ARCHIVE=$BASENAME$DATE.tar.gz
else
    ARCHIVE=$(ls -rt $BASENAME*.tar.gz | tail -n1)
fi

restore()
{
    echo "Restoring configuration files"
    tar -Pxzvf $ARCHIVE

    echo "Restarting dansguardian"
    yourpath/servicectl.sh restart dansguardian

    echo "Please be more careful next time. :)"
}


if [ -z $ARCHIVE ]
then
    echo "ERROR: No valid archive found or given."
    exit 1
fi

if ! [ -s $ARCHIVE ]
then
    echo "ERROR: $ARCHIVE does not exist or is empty."
    exit 1
fi

echo "Restore $ARCHIVE? (y/n)"
read ans
case $ans in
  y|Y|yes) restore && exit 0;;
  *) echo "Exiting" && exit 1;;
esac
