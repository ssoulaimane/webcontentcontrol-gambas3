#! /bin/bash

if [ $# -ne 4 ]
then
	echo "usage : $0 Hmin Mmin Hmax Mmax"
	exit 0 
fi

Hmin=$1
Mmin=$2

Hmax=$3
Mmax=$4

replace_strings()
{
	sed -i s/Hmin/$Hmin/g $1
	sed -i s/Mmin/$Mmin/g $1
	sed -i s/Hmax/$Hmax/g $1
	sed -i s/Mmax/$Mmax/g $1
}

#get /etc/rc.local in here
echo "-->making local copy of /etc/rc.local"
cp /etc/rc.local .

#make backups
echo "-->making backups"
cp rc.local rc.local.$(date +%Y%m%d_%H%M%S)

#overwrite the files with their templates
echo "-->overwriting files with their templates"
cp rc.local.uptime.template rc.local

#replace the time strings
echo "-->replacing strings"
replace_strings rc.local

#overwrite /etc/rc.local with the new version
echo "-->replacing /etc/rc.local with new version"
cp yourpath/rc.local /etc/rc.local

#run /etc/rc.local to activate protection immediately (and test it of course :) )
echo "-->Activating protection"
/etc/rc.local
