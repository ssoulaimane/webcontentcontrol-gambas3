#!/bin/bash

# echo list1=$@
# echo list2=$*

for url in $@;
do
        echo "Adding $url"
	sed -i '/#List other sites to block:/a\'$url'' /etc/dansguardian/lists/bannedsitelist >/dev/null
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
