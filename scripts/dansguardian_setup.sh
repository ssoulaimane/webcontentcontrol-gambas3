#!/bin/bash
source yourpath/file_paths.sh

status()
{
    if grep -q ^UNCONFIGURED $DG || grep -q ^#daemonuser $DG || grep -q ^#daemongroup $DG
    then
	echo "STATUS: Dansguardian is unconfigured"
	return 0
    else
	echo "STATUS: Dansguardian is configured"
	return 1
    fi
}

config_set()
{
    name=$1
    value=$2
    if grep -q "^$name = " $DG && sed -i "s/^$name = .*/$name = $value/" $DG
    then
        return
    # ..................................... replace first match only
    elif grep -q "^#$name = " $DG && sed -i "0,/^#$name = / s/^#$name = .*/$name = $value/" $DG
    then
        return
    else
        echo "$name = $value" >> $DG
        return
    fi
}

if [ $# -ne 1 ]
then
        echo "usage : $0 <on/off/status>"
        exit 0
fi

######### VARS #########
USER=webcontentcontrol
GROUP=webcontentcontrol
########################

if [ $1 = 'on' ]
then
    status
    sed -i 's/^UNCONFIGURED/#UNCONFIGURED/'  $DG >/dev/null
    config_set daemonuser "'$USER'"
    config_set daemongroup "'$GROUP'"
    status
    if [ $? -eq 1 ]
    then
	echo "Configuration successful"
    else
	echo "Configuration not successful"
    fi
fi

if [ $1 = 'off' ]
then
    status
    sed -i 's/^#UNCONFIGURED/UNCONFIGURED/' $DG >/dev/null
    sed -i 's/^daemonuser/#daemonuser/'  $DG >/dev/null
    sed -i 's/^daemongroup/#daemongroup/'  $DG >/dev/null
    status
    if [ $? -eq 0 ]
    then
	echo "Unconfiguration successful"
    else
	echo "Unconfiguration not successful"
    fi
fi

if [ $1 = 'status' ]
then
    status
fi
