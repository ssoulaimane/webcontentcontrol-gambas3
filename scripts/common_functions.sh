#!/bin/bash

function check_fh {
    FIREHOL_LOCK_DIR="/var/lock/subsys"
    test ! -d "${FIREHOL_LOCK_DIR}" && FIREHOL_LOCK_DIR="/var/lock"

    ## FireHol
    if ( test -n "`which firehol`" ) && ( test -f $FIREHOL_LOCK_DIR/firehol || test -f /var/lock/subsys/firehol );
    then fh_running=1 && echo "FireHol is running" && return 1;
    else fh_running=0 && echo "FireHol is stopped" && return 0;
    fi
}

function check_tp {
    ## TinyProxy
    pid_tp="`pidof tinyproxy | tr -d [:space:]`"
    if (test -z $pid_tp);
    then tp_running=0 && echo "TinyProxy is stopped" && return 0;
    else tp_running=1 && echo "TinyProxy is running" && return 1;
    fi
}

function check_dg {
    ## DansGuardian
    pid_dg="`pidof dansguardian | tr -d [:space:]`"
    if (test -z $pid_dg);
    then dg_running=0 && echo "DansGuardian is stopped" && return 0;
    else dg_running=1 && echo "DansGuardian is running" && return 1;
    fi
}

function check_sq {
    ## squid
    pid="`pidof squid | tr -d [:space:]`"
    if (test -z $pid);
    then sq_running=0 && echo "squid is stopped" && return 0;
    else sq_running=1 && echo "squid is running" && return 1;
    fi
}

function check_all {
    check_fh
    check_tp
    check_dg
}

function restart_all_nogui {
    yourpath/servicectl.sh restart firehol;
    yourpath/servicectl.sh restart tinyproxy;
    yourpath/servicectl.sh restart dansguardian;
}

function stop_all {
    gksudo yourpath/servicectl.sh stop firehol;
    gksudo yourpath/servicectl.sh stop tinyproxy;
    gksudo yourpath/servicectl.sh stop dansguardian;
}

# called nogui because it does not require gksudo (which might not be available on all systems)
function start_all_nogui {
    yourpath/servicectl.sh start firehol;
    yourpath/servicectl.sh start tinyproxy;
    yourpath/servicectl.sh start dansguardian;
}

function stop_all_nogui {
    yourpath/servicectl.sh stop firehol;
    yourpath/servicectl.sh stop tinyproxy;
    yourpath/servicectl.sh stop dansguardian;
}

function startcheck {
    check_all
    if (test $fh_running -eq 1) && (test $tp_running -eq 1) && (test $dg_running -eq 1);
    then
	echo "All OK! Everything is running"
	return 0
    else
	echo "There was an error"
	return 1
    fi
}

function stopcheck {
    check_all
    if (test $fh_running -eq 0) && (test $tp_running -eq 0) && (test $dg_running -eq 0);
    then
	echo "All OK! Everything was stopped"
	return 0
    else
	echo "There was an error"
	return 1
    fi
}

ProcessAllow()
{
    FILE=$1
    USERNAME=$2
    TMP=$(mktemp)
    cp -v $FILE $TMP
    sed -i '/^$/ d' $TMP
    sed -i '/^#/ d' $TMP
    nlines=$(wc -l $TMP | awk '{print $1}')
    echo nlines=$nlines
    echo "=============="
    cat $TMP
    echo "=============="
    if [ $nlines -ne 0 ]
    then
	echo "Processing groups from $FILE"
	#add user to allowed groups
	cat $TMP | xargs -n1 yourpath/add_user_to_group.sh $USERNAME
	return 0
    else
	echo "$FILE does not contain any groups"
	return 1
    fi
}

ProcessBan()
{
    FILE=$1
    USERNAME=$2
    TMP=$(mktemp)
    cp -v $FILE $TMP
    sed -i '/^$/ d' $TMP
    sed -i '/^#/ d' $TMP
    nlines=$(wc -l $TMP | awk '{print $1}')
    echo nlines=$nlines
    echo "=============="
    cat $TMP
    echo "=============="
    if [ $nlines -ne 0 ]
    then
	echo "Processing groups from $FILE"
	#remove user from banned groups
	cat $TMP | xargs -n1 yourpath/remove_user_from_group.sh $USERNAME
	return 0
    else
	echo "$FILE does not contain any groups"
	return 1
    fi
}
