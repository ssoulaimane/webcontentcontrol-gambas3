#!/bin/bash
set -e -u

if [ $# -ne 2 ]
then
	echo "usage : $0 <user> <group>"
	exit -1 
fi
group=$2
username=$1

echo "Removing $username from $group"
grouplist=`id -nG $username | sed -e 's/'$group'//g' -e 's/ \+/,/g' -e 's/^,//' -e 's/,$//'`
sudo usermod -G $grouplist $username
