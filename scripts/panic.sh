#!/usr/bin/env bash

set -x

lockdown()
{
  source /usr/share/webcontentcontrol/scripts/file_paths.sh
  source /usr/share/webcontentcontrol/scripts/import_export_functions.sh
  source /usr/share/webcontentcontrol/scripts/common_functions.sh

  CRONTAB_TEMPLATE=$HOME/panicbutton/crontab.locked.template
  CRONTAB_FINAL=/usr/share/webcontentcontrol/scripts/crontab.locked

  USERNAME=$USER
  ZEFILE=/WCCsafe/HomeKey.txt.gpg
  M="\*"
  H="\*"
  DOM="\*"
  MON="\*"
  DOW=$(expr $(date +%u) + 1)
  echo DOW=$DOW

  ##########################
  #create sudoers.locked
  sed "/# User alias specification/a\User_Alias     PARTIALADMINS = $USERNAME" /usr/share/webcontentcontrol/scripts/sudoers.locked.template >/usr/share/webcontentcontrol/scripts/sudoers.locked

  #check sudoers.locked
  if ! ( visudo -c -f /usr/share/webcontentcontrol/scripts/sudoers.locked )
  then
      echo "ERROR: /usr/share/webcontentcontrol/scripts/sudoers.locked was not parsed successfully."
      exit 1
  fi
  ##########################

  ##########################
  #locked groups
  echo "-->locking groups"
  /usr/share/webcontentcontrol/scripts/remove_user_from_group.sh $USERNAME gamer
  /usr/share/webcontentcontrol/scripts/remove_user_from_group.sh $USERNAME programmer
  /usr/share/webcontentcontrol/scripts/remove_user_from_group.sh $USERNAME browsers
  ##########################

  ##########################
  #backup files
  echo "-->making backups"
  cp -v /etc/sudoers /usr/share/webcontentcontrol/scripts/sudoers.$(date +%Y%m%d_%H%M%S)
  cp -v /boot/grub/menu.lst /usr/share/webcontentcontrol/scripts/menu.lst.$(date +%Y%m%d_%H%M%S)
  ##########################

  ##########################
  #replace files
  echo "-->Locking system"
  #cp -v /usr/share/webcontentcontrol/scripts/menu.lst.locked /boot/grub/menu.lst
  sed -i s/^#password/password/ /boot/grub/menu.lst
  cp -v /usr/share/webcontentcontrol/scripts/sudoers.locked /etc/sudoers
  ##########################

  ##########################
  #lock proxy settings
  echo "-->Locking firefox proxy settings"
  /usr/share/webcontentcontrol/scripts/firefox_setup.sh lock
  ##########################

  ##########################
  #lock WPA
  echo "-->Locking WPA interfaces"
  /usr/share/webcontentcontrol/scripts/ifupdown_setup.sh lock
  ##########################

  ##########################
  #Make sure all services run at startup
  echo "-->Activating services"
  /usr/share/webcontentcontrol/scripts/service_setup.sh firehol on
  /usr/share/webcontentcontrol/scripts/service_setup.sh tinyproxy on
  /usr/share/webcontentcontrol/scripts/service_setup.sh dansguardian on
  ##########################

  ##########################
  #restart services
  echo "-->Restarting all services"
  restart_all_nogui
  ##########################

  ##########################
  #time-lock
  if [ ! -z $ZEFILE ]
  then
      if [ ! -s $ZEFILE ]
      then
	  echo "ERROR: $ZEFILE does not exist or is empty."
	  exit 1
      fi
      echo "adding $ZEFILE unlocking to crontab"
      sed "\$a $M $H $DOM $MON $DOW chmod 755 $ZEFILE" $CRONTAB_TEMPLATE >$CRONTAB_FINAL
      echo "chmod 000 $ZEFILE"
      chmod 000 $ZEFILE
  else
      cp -v $CRONTAB_TEMPLATE $CRONTAB_FINAL
  fi
  echo "================"
  cat $CRONTAB_TEMPLATE
  echo "================"
  cat $CRONTAB_FINAL
  echo "================"

  echo "-->activating time lock"
  crontab $CRONTAB_FINAL
  ##########################

  echo "System lockdown successful."
}

if zenity  --question --title "Alert"  --text "Activate emergency modus?"
then
  echo "Activating emergency modus..."
  lockdown >$HOME/lockdown.log
else
  echo "False alert"
fi
