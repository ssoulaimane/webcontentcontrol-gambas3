#!/bin/bash
set -u

source yourpath/file_paths.sh
source yourpath/import_export_functions.sh
source yourpath/common_functions.sh

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` user"
        exit 0
fi

USERNAME=$1

modif=0

#restart all services
restart_all_nogui

#apply group permissions
if ProcessAllow $TC_ALLOWED $USERNAME
then
    echo "ProcessAllow OK"
    modif=1
fi

if ProcessBan $TC_BANNED $USERNAME
then
    echo "ProcessBan OK"
    modif=1
fi

if [ $modif -eq 1 ]
then
    echo "You need to log out and in again for the program permissions to take effect." >&2
fi
