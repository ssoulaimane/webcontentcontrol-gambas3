#! /bin/bash -e
#This script shuts down the PC if Hmin:Mmin<=H:M<=Hmax:Mmax where H:M is the current time.
#Otherwise it launches a shutdown -h Hmin:Mmin command

if [ $# -ne 4 ]
then
	echo "This script shuts down the PC if Hmin:Mmin<=H:M<=Hmax:Mmax where H:M is the current time."
	echo "Otherwise it launches a \"shutdown -h Hmin:Mmin\" command"
	echo "usage : $0 Hmin Mmin Hmax Mmax"
	exit 0 
fi

LOGFILE=/var/log/computercontrol.log

Hmin=$1
Mmin=$2

Hmax=$3
Mmax=$4

print_and_log()
{
    echo $1
    echo $1 >>$LOGFILE 2>&1
}

if ( /usr/local/bin/timewindow.sh $Hmin $Mmin $Hmax $Mmax )
then
        print_and_log "PC usage forbidden"
        print_and_log "Shutting down now!"
        shutdown -h now 1>>$LOGFILE 2>&1 &
else
        print_and_log "PC usage allowed"
        print_and_log "Scheduling shutdown"
        shutdown -h $Hmin:$Mmin 1>>$LOGFILE 2>&1 &
	print_and_log "Shutdown scheduled succesfully! :D"
fi;

echo "script exited successfully"
