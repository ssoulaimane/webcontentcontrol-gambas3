#! /bin/bash -e
#This script shuts down the PC if Hmin:Mmin<=H:M<=Hmax:Mmax where H:M is the current time.
#Otherwise it launches a shutdown -h Hmin:Mmin command

if [ $# -ne 4 ]
then
	echo "This script shuts down the PC if Hmin:Mmin<=H:M<=Hmax:Mmax where H:M is the current time."
	echo "Otherwise it launches a \"shutdown -h Hmin:Mmin\" command"
	echo "usage : $0 Hmin Mmin Hmax Mmax"
	exit 0 
fi

H=`date +%H`
M=`date +%M`

Hmin=$1
Mmin=$2

Hmax=$3
Mmax=$4

T=`expr 60 \* $H + $M`
Tmin=`expr 60 \* $Hmin + $Mmin`
Tmax=`expr 60 \* $Hmax + $Mmax`

echo $T
echo $Tmin
echo $Tmax

if [ $Tmin -le $T ] && [ $T -le $Tmax ];
then
	#zenity --info --text "Shutting down now!" &
	echo "shutdown -h now &" >>~/computercontrol.log &
	shutdown -h now &
else
	#zenity --info --text "Shutdown scheduled for $Hmin:$Mmin" &
	echo "shutdown -h $Hmin:$Mmin &" >>~/computercontrol.log &
	shutdown -h $Hmin:$Mmin &
fi;

echo "Shutdown scheduled succesfully! :D"
