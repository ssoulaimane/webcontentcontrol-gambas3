#! /bin/bash
#take down all network interfaces
/sbin/ifdown -a
#eject wifi card
/sbin/pccardctl eject 0
#stop dhcdbd daemon
yourpath/servicectl.sh stop dhcdbd
#change ip address
/sbin/ifconfig eth0 192.168.123.45
/sbin/ifconfig ath0 192.168.123.45
/sbin/ifconfig wifi0 192.168.123.45
