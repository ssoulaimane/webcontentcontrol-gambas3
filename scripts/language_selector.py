#!/usr/bin/env python2

import pygtk
pygtk.require('2.0')
import gtk
import os

class ComboBoxExample:
    def __init__(self):
	#initialize self.lang
	self.lang="ukenglish"

	#create window and make close button work
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        #window.connect('destroy', lambda w: gtk.main_quit())
        window.connect("destroy", self.destroy)

	#set window title
        window.set_title("Language selector")

	#create VBox
	box = gtk.VBox(False, 0)
        window.add(box)

	#create label
	label = gtk.Label("Please select dansguardian's language.\nThis will be the language of the access denied page, not of the wordlist used by the filter.")
        box.pack_start(label, True, True, 0)

	#create dropdown list
	combobox = gtk.combo_box_new_text()
        box.pack_start(combobox, True, True, 0)

	#create OK button
	button = gtk.Button("OK")
        box.pack_start(button, True, True, 0)
	
	#fill combobox with language list
	combobox.append_text('Language')
        in_file=open("/tmp/langlist",'r')
	for line in in_file:
	    line = line.rstrip('\n')
	    combobox.append_text(line)
	in_file.close()

	#connect changed_cb function to the combobox
	combobox.connect('changed', self.changed_cb)
        combobox.set_active(0)

	#call self.destroy when clicked
        button.connect("clicked", self.destroy)
	
	#show window
        window.show_all()

	#center window on screen
	window_size=window.get_size()
	window.move((gtk.gdk.screen_width()-window_size[0])/2, (gtk.gdk.screen_height()-window_size[1])/2)

	return

    #function to update self.lang
    def changed_cb(self, combobox):
        model = combobox.get_model()
        index = combobox.get_active()
        if index:
            self.lang=model[index][0]
        return

    #function to "return" self.lang and quit
    def destroy(self, widget, data=None):
	print self.lang
        gtk.main_quit()

def main():
    gtk.main()
    return

if __name__ == "__main__":
    #create a text file with the list of available languages
    cmd='cd /etc/dansguardian/languages/ && ls -d -A * >/tmp/langlist'
    os.system(cmd)

    #create and launch the GUI
    bcb = ComboBoxExample()
    main()
