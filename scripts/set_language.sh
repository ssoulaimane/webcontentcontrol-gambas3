#!/bin/bash
source yourpath/file_paths.sh

if [ $# -ne 1 ]
then
	echo "usage : $0 <language>"
	exit 0 
fi

language=`sed -n "s/language = '\(.*\)'/\1/p" $DG`
echo language=$language

sed -i.bak "s/language = '.*'/language = '$1'/" $DG

echo "========================"
language=`sed -n "s/language = '\(.*\)'/\1/p" $DG`
echo language=$language
