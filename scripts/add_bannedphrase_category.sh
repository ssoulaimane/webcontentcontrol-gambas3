#!/bin/bash

FILE=/etc/dansguardian/lists/bannedphraselist

for category in $@;
do
	#make sure it's there to avoid dansguardian not starting
	if test -s /etc/dansguardian/lists/phraselists/$category/banned
	then
	    echo "Adding $category"
	    sed -i '$a .Include</etc/dansguardian/lists/phraselists/'$category'/banned>'  $FILE >/dev/null
	else
	    echo "ERROR: Category not found"
	fi
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
