#!/bin/bash

source yourpath/import_export_functions.sh

if [ "$1" = '-h' ] || [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` ARCHIVE"
        exit 0
fi

import_settings "$@"
