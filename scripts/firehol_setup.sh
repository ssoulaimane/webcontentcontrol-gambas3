#!/bin/bash
source yourpath/file_paths.sh

insert_version()
{
line_nb=`sed -n -e '/^#/!=' -e '/^#/!q' $FH`
if test -z $line_nb
then
line_nb=`wc -l <$FH`
sed -i.bak ''$line_nb' a\version 5' $FH
else
sed -i.bak ''$line_nb' i\version 5' $FH
fi
}
insert_iptables()
{
sed -i.bak '
/version 5/a\
iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport 3128 -m owner ! --uid-owner webcontentcontrol -j DROP
' $FH
}
insert_transparent_squid()
{
sed -i.bak '
/iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport 3128 -m owner ! --uid-owner webcontentcontrol -j DROP/a\
transparent_squid 8080 "webcontentcontrol root"
' $FH
}
insert_interface()
{
sed -i.bak '
/transparent_squid 8080 "webcontentcontrol root"/a\
interface any world
' $FH
}
insert_policy()
{
sed -i.bak '
/interface any world/a\
policy drop
' $FH
}
insert_protection()
{
sed -i.bak '
/policy drop/a\
protection strong
' $FH
}
insert_client()
{
sed -i.bak '
/protection strong/a\
client all accept
' $FH
}
insert_server()
{
sed -i.bak '
/client all accept/a\
server cups accept
' $FH
}

remove_iptables()
{
sed -i.bak '
/iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport 3128 -m owner ! --uid-owner webcontentcontrol -j DROP/ d
' $FH
}
remove_transparent_squid()
{
sed -i.bak '
/transparent_squid 8080 "webcontentcontrol root"/ d
' $FH
}
remove_policy()
{
sed -i.bak '
/policy drop/ d
' $FH
}
remove_protection()
{
sed -i.bak '
/protection strong/ d
' $FH
}
remove_server()
{
sed -i.bak '
/server cups accept/ d
' $FH
}

if [ $# -ne 1 ]
then
        echo "usage : $0 <on/off/status>"
        exit 0
fi


if grep 'version 5' $FH
then
    echo "PRESENT"
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_version; fi
fi

if grep 'iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport [0-9]* -m owner ! --uid-owner webcontentcontrol -j DROP' $FH
then
    echo "PRESENT"
    if [ $1 = 'off' ]; then remove_iptables; fi
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_iptables; fi
fi

if grep 'transparent_squid [0-9]* "webcontentcontrol root"' $FH
then
    echo "PRESENT"
    if [ $1 = 'off' ]; then remove_transparent_squid; fi
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_transparent_squid; fi
fi

if grep 'interface any world' $FH
then
    echo "PRESENT"
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_interface; fi
fi

if grep 'policy drop' $FH
then
    echo "PRESENT"
    if [ $1 = 'off' ]; then remove_policy; fi
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_policy; fi
fi

if grep 'protection strong' $FH
then
    echo "PRESENT"
    if [ $1 = 'off' ]; then remove_protection; fi
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_protection; fi
fi

if grep 'client all accept' $FH
then
    echo "PRESENT"
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_client; fi
fi

if grep 'server cups accept' $FH
then
    echo "PRESENT"
    if [ $1 = 'off' ]; then remove_server; fi
else
    echo "MISSING"
    if [ $1 = 'on' ]; then insert_server; fi
fi
