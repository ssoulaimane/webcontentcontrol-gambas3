#!/bin/bash
#Unconfigures everything that was configured through install.sh

if [ $# -ne 1 ]
then
        echo "usage : $0 <maindir>"
        exit 1
fi

MAINDIR=$1

# We must use ./ here because this script is called after uninstall
# Careful! "yourpath" is not defined!
source ./file_paths.sh
source ./common_functions.sh

FH=/etc/firehol/firehol.conf
remove_iptables()
{
sed -i.bak '
/iptables -t filter -I OUTPUT -d 127.0.0.1 -p tcp --dport 3128 -m owner ! --uid-owner webcontentcontrol -j DROP/ d
' $FH
}
remove_transparent_squid()
{
sed -i.bak '
/transparent_squid 8080 "webcontentcontrol root"/ d
' $FH
}
remove_policy()
{
sed -i.bak '
/policy drop/ d
' $FH
}
remove_protection()
{
sed -i.bak '
/protection strong/ d
' $FH
}
remove_server()
{
sed -i.bak '
/server cups accept/ d
' $FH
}

# stop everything
stop_all_nogui
stopcheck

# remove firehol settings related to WCC (TODO: restore from preinstall-backup?)
if [ -f $FH ]
then
  remove_iptables
  remove_transparent_squid
  remove_policy
  remove_protection
  remove_server
else
  echo "WARNING: FAILED TO PROPERLY DEACTIVATE FIREHOL!!!"
fi
# prevent firehol from running at startup (to make sure net works after removal)
if [ -f /etc/default/firehol ]
then
  sed -i.bak 's/START_FIREHOL=.*/START_FIREHOL=NO/' /etc/default/firehol
else
  echo "WARNING: FAILED TO PROPERLY DEACTIVATE FIREHOL!!!"
fi

# remove custom group:user if exists
getent passwd webcontentcontrol > /dev/null && userdel  webcontentcontrol
getent group  webcontentcontrol > /dev/null && groupdel webcontentcontrol

rm /tmp/.dguardianipc /tmp/.dguardianurlipc  # resolve some dansguardian ownership problems

# restore settings from preinstall-backup
./import_all.sh "$1/backup"

# restore ownership
TP_USER=`grep '^User\s' $TP | awk '{print $2}'`
TP_GROUP=`grep '^Group\s' $TP | awk '{print $2}'`
chown -R $TP_USER:$TP_GROUP $TP_LOGDIR /var/run/tinyproxy

DG_USER=`sed -n "s/^daemonuser = '\(.*\)'/\1/p" $DG`
DG_GROUP=`sed -n "s/^daemongroup = '\(.*\)'/\1/p" $DG`
chown -R $DG_USER:$DG_GROUP $DG_LOGDIR

# remove leftovers
rm $MAINDIR/configured
rm $MAINDIR/*.dgs.full
