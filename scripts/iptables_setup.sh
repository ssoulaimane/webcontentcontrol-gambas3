#!/bin/bash -e
iptables -t filter -A OUTPUT -p tcp --dport 80 -m owner --uid-owner webcontentcontrol -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 3128 -m owner --uid-owner webcontentcontrol -j ACCEPT
#iptables -t filter -A OUTPUT -p tcp --dport 80 -m owner --uid-owner EXEMPT_USER -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 80 -j REDIRECT --to-ports 8080
iptables -t filter -A OUTPUT -p tcp --dport 3128 -j REDIRECT --to-ports 8080

#iptables-save > /etc/sysconfig/iptables

#chkconfig iptables on
#service iptables restart

echo "ALL DONE"
