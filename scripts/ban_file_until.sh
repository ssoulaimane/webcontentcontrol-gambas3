#!/bin/bash
set -e -u

TC_PATH=yourpath

# Check if all parameters are present
# If no, exit
if [ $# -ne 2 ]
then
        echo "usage :"
        echo "`basename $0` PROGRAM HH:MM"
        exit 0
fi

#readlink!!! Exactly what I needed! :D Will need to improve a lot of other scripts now. :D
program_path=$( readlink -f $1 )
TIME=$2

echo "program_path=$program_path"
echo "TIME=$TIME"

#ban
echo "banning program"
#o-rx is not dangerous
#g-rx allows deactivating programs that are already controlled
chmod g-rx,o-rx $program_path
#unban
echo "setting timed unban"
command="chmod g+rx,o+rx $program_path"
echo "$command" | at $TIME
