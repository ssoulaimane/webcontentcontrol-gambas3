#!/bin/bash

for category in $@;
do
	#make sure it's there to avoid dansguardian not starting
	if test -s /etc/dansguardian/lists/blacklists/$category/domains
	then
	    echo "Adding $category"
	    sed -i '$a .Include</etc/dansguardian/lists/blacklists/'$category'/domains>'  /etc/dansguardian/lists/bannedsitelist >/dev/null
	else
	    echo "ERROR: Category not found"
	fi
done

echo "restarting dansguardian"
yourpath/servicectl.sh restart dansguardian
