#!/bin/bash

if [ $# -ne 2 ]
then
	echo "usage : $0 <program> <group>"
	exit 0 
fi
program=$1
group=$2

program_path=`which $program`
echo $program_path

if test -z $program_path
then
echo "empty program_path"
exit 1
fi
if test -z $group 
then
echo "empty group"
exit 1
fi

ls -l $program_path
#g-rw,o-rwx not only prevents execution, but also copying to the user's home ;)
sudo chgrp $group $program_path
if ( file $program_path | grep text )
then
	echo "This is a script. Keeping read permissions."
	chmod g=rx,o-rwx $program_path
else
	echo "Normal binary. Removing read permissions."
	chmod g=x,o-rwx $program_path
fi
ls -l $program_path
