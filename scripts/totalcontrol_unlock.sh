#!/bin/bash

source yourpath/file_paths.sh
source yourpath/import_export_functions.sh
source yourpath/common_functions.sh

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` [USERNAME]"
        exit 0
fi

USERNAME=$1

#locked groups
echo "-->unlocking groups"
yourpath/add_user_to_group.sh $USERNAME gamer
yourpath/add_user_to_group.sh $USERNAME programmer
yourpath/add_user_to_group.sh $USERNAME browsers

#backup files
echo "-->making backups"
cp -v /etc/sudoers yourpath/sudoers.$(date +%Y%m%d_%H%M%S)
cp -v /boot/grub/menu.lst yourpath/menu.lst.$(date +%Y%m%d_%H%M%S)

#replace files
echo "-->Unlocking system"
#cp -v yourpath/menu.lst.unlocked /boot/grub/menu.lst
sed -i s/^password/#password/ /boot/grub/menu.lst
cp -v yourpath/sudoers.unlocked /etc/sudoers

#unlock proxy settings
echo "-->Unlocking firefox proxy settings"
yourpath/firefox_setup.sh unlock

#unlock WPA
echo "-->Unlocking WPA interfaces"
yourpath/ifupdown_setup.sh unlock

#Make sure all services don't run at startup
echo "-->Deactivating services"
yourpath/service_setup.sh firehol off
yourpath/service_setup.sh tinyproxy off
yourpath/service_setup.sh dansguardian off

#Stop services
echo "-->Stopping all services"
stop_all_nogui

echo "Would you like to change the root password?"
read ans
case $ans in
        y|Y|yes) passwd root;;
esac

echo "System unlocked successfully."
