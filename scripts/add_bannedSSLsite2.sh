#!/bin/bash

# echo list1=$@
# echo list2=$*

for url in $@;
do
        echo "Adding $url"
	#Add URL at the end of the line. :)
	sed -i 's/acl sucky-proxy dstdomain.*$/& '$url'/' /etc/squid/squid.conf >/dev/null
done

echo "restarting squid"
yourpath/servicectl.sh restart squid
