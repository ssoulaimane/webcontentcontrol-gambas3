#!/bin/bash

FILE=/etc/dansguardian/lists/bannedregexpurllist

# Check if all parameters are present
# If no, exit
if [ $# -ne 1 ]
then
        echo "usage :"
        echo "`basename $0` [FILE]"
	echo "This script will attach the contents of FILE to $FILE"
        exit 0
fi

if ! [ -s $1 ]
then
    echo "ERROR: $1 does not exist or is empty." && exit 1
fi

add_it()
{
    if ! [ -s $TMP ]
    then
	echo "ERROR: $TMP does not exist or is empty." && exit 1
    fi
    cat $TMP >>$FILE
    echo "restarting dansguardian"
    yourpath/servicectl.sh restart dansguardian
}

###########################
#Do some file processing eventually to avoid problems
TMP=`mktemp` || ( echo "ERROR: Could not create temporary file." && exit 1 )

#check that all temporary files are read-write
if ! ( [ -r $TMP ] && [ -w $TMP ] )
then
    echo "ERROR: $TMP does not have read/write permissions"
    exit -1
fi

cat $1 >$TMP
###########################

echo "========LINES TO ADD========"
cat $TMP
echo "============================"

echo "Add those lines to $FILE?"
read ans
case $ans in
  y|Y|yes) add_it && exit 0;;
  *) exit 1;;
esac

exit 2
