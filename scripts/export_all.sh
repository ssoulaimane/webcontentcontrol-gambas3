#!/bin/bash
source yourpath/file_paths.sh

if [ $# -ne 1 ]
then
	echo "usage : $0 <filename>"
	exit 0
fi

DIR=$(dirname "$1")
filename=$DIR/$(basename "$1" .dgs.full).dgs.full

echo "Running from $(pwd)"
echo "===>exporting $filename"
tar -Pczvf "$filename" $DG $FH $TP $DG_INI $FH_INI $SQ $SQ3 $SQG $SQG_DOMS $DG_LISTS $TP_FILTER $FF_CFG $FF_JS $FF_JS1 $FF_JS2 $FF_JS3 $FF_JS4 $FF_CFG1 $FF_CFG2
echo "Running from $(pwd)"
echo "===>exported $filename"
